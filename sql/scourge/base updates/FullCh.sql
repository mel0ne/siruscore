ALTER TABLE `corpse` CHANGE COLUMN `phaseMask` `phaseMask` smallint(5) unsigned NOT NULL DEFAULT '1';
ALTER TABLE `game_event_save` CHANGE `event_id` `eventEntry` TINYINT(3) UNSIGNED NOT NULL;
ALTER TABLE `game_event_condition_save` CHANGE `event_id` `eventEntry` TINYINT(3) UNSIGNED NOT NULL;
-- Update arena_team table
ALTER TABLE `arena_team`
CHANGE `arenateamid` `arenaTeamId` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `captainguid` `captainGuid` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `BackgroundColor` `backgroundColor` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `EmblemStyle` `emblemStyle` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `EmblemColor` `emblemColor` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `BorderStyle` `borderStyle` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `BorderColor` `borderColor` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
ADD COLUMN `rating` SMALLINT(5) UNSIGNED NOT NULL AFTER `type`,
ADD COLUMN `seasonGames` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL AFTER `rating`,
ADD COLUMN `seasonWins` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL AFTER `seasonGames`,
ADD COLUMN `weekGames` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL AFTER `seasonWins`,
ADD COLUMN `weekWins` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL AFTER `weekGames`,
ADD COLUMN `rank` INT(10) UNSIGNED NOT NULL AFTER `weekWins`;

-- Move data from arena_team_stats to new columns in arena_team
UPDATE `arena_team` a, `arena_team_stats` s SET
`a`.`rating` = `s`.`rating`,
`a`.`seasonGames` = `s`.`played`,
`a`.`seasonWins` = `s`.`wins2`,
`a`.`weekGames` = `s`.`games`,
`a`.`weekWins` = `s`.`wins`,
`a`.`rank` = `s`.`rank`
WHERE `a`.`arenaTeamId` = `s`.`arenateamid`;

-- Remove arena_team_stats table
DROP TABLE `arena_team_stats`;

-- Update arena_team_member table
ALTER TABLE `arena_team_member`
CHANGE `arenateamid` `arenaTeamId` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `played_week` `weekGames` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `wons_week` `weekWins` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `played_season` `seasonGames` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `wons_season` `seasonWins` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL,
ADD COLUMN `personalRating` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL AFTER `seasonWins`;

-- Update character_arena_stats table
ALTER TABLE `character_arena_stats`
CHANGE `personal_rating` `personalRating` SMALLINT(5) NOT NULL,
CHANGE `matchmaker_rating` `matchMakerRating` SMALLINT(5) NOT NULL;

-- Update arena_team_member table
ALTER TABLE `arena_team_member`
DROP COLUMN `personalRating`;
ALTER TABLE `character_battleground_data`
    CHANGE `instance_id` `instanceId` int(10) unsigned NOT NULL COMMENT 'Instance Identifier',
    CHANGE `join_map` `joinMapId` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Map Identifier',
    CHANGE `join_x` `joinX` float NOT NULL DEFAULT '0',
    CHANGE `join_y` `joinY` float NOT NULL DEFAULT '0',
    CHANGE `join_z` `joinZ` float NOT NULL DEFAULT '0',
    CHANGE `join_o` `joinO` float NOT NULL DEFAULT '0',
    CHANGE `taxi_start` `taxiStart` int(10) unsigned NOT NULL DEFAULT '0',
    CHANGE `taxi_end` `taxiEnd` int(10) unsigned NOT NULL DEFAULT '0',
    CHANGE `mount_spell` `mountSpell` mediumint(8) unsigned NOT NULL DEFAULT '0';

ALTER TABLE `character_homebind`
    CHANGE `map` `mapId` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Map Identifier',
    CHANGE `zone` `zoneId` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Zone Identifier',
    CHANGE `position_x` `posX` float NOT NULL DEFAULT '0',
    CHANGE `position_y` `posY` float NOT NULL DEFAULT '0',
    CHANGE `position_z` `posZ` float NOT NULL DEFAULT '0';

ALTER TABLE `arena_team`
CHANGE `rating` `rating` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0',
CHANGE `rank` `rank` INT(10) UNSIGNED NOT NULL DEFAULT '0';

ALTER TABLE `arena_team_member`
ADD COLUMN `personalRating` SMALLINT(5) NOT NULL DEFAULT '0' AFTER guid;

UPDATE arena_team_member
INNER JOIN character_arena_stats ON arena_team_member.guid = character_arena_stats.guid
INNER JOIN arena_team ON arena_team.arenaTeamId = arena_team_member.arenaTeamId AND (slot = 0 AND TYPE = 2 OR slot = 1 AND TYPE = 3 OR slot = 2 AND TYPE = 5)
SET arena_team_member.personalRating = character_arena_stats.personalRating;

ALTER TABLE `character_arena_stats`
DROP COLUMN `personalRating`;

ALTER TABLE `corpse`
    CHANGE `guid` `corpseGuid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier',
    CHANGE `player` `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Character Global Unique Identifier',
    CHANGE `map` `mapId` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Map Identifier',
    CHANGE `position_x` `posX` float NOT NULL DEFAULT '0',
    CHANGE `position_y` `posY` float NOT NULL DEFAULT '0',
    CHANGE `position_z` `posZ` float NOT NULL DEFAULT '0',
    CHANGE `guild` `guildId` int(10) unsigned NOT NULL DEFAULT '0',
    CHANGE `corpse_type` `corpseType` tinyint(3) unsigned NOT NULL DEFAULT '0',
    CHANGE `instance` `instanceId` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Instance Identifier';

ALTER TABLE `creature_respawn`
    CHANGE `respawntime` `respawnTime` int(10) unsigned NOT NULL DEFAULT '0',
    CHANGE `instance` `instanceId` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Instance Identifier';

ALTER TABLE `gameobject_respawn`
    CHANGE `respawntime` `respawnTime` int(10) unsigned NOT NULL DEFAULT '0',
    CHANGE `instance` `instanceId` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Instance Identifier';
ALTER TABLE corpse
    DROP PRIMARY KEY,
    DROP KEY `idx_type`,
    DROP KEY `instance`,
    DROP KEY `Idx_player`,
    DROP KEY `Idx_time`;

ALTER TABLE corpse
    ADD PRIMARY KEY (`corpseGuid`),
    ADD KEY `idx_type`(`corpseType`),
    ADD KEY `idx_instance`(`instanceId`),
    ADD KEY `idx_player`(`guid`),
    ADD KEY `idx_time`(`time`);

ALTER TABLE creature_respawn
    DROP PRIMARY KEY,
    DROP KEY `instance`;

ALTER TABLE creature_respawn
    ADD PRIMARY KEY (`guid`, `instanceId`),
    ADD KEY `idx_instance`(`instanceId`);

ALTER TABLE gameobject_respawn
    DROP PRIMARY KEY,
    DROP KEY `instance`;

ALTER TABLE gameobject_respawn
    ADD PRIMARY KEY (`guid`, `instanceId`),
    ADD KEY `idx_instance`(`instanceId`);

-- append extra zeros, only if the string is properly formatted (has 38 tokens)
UPDATE `characters` SET `equipmentCache`=CONCAT(`equipmentCache`,"0 0 0 0 0 0 0 0 ") WHERE (CHAR_LENGTH(`equipmentCache`) - CHAR_LENGTH(REPLACE(`equipmentCache`, ' ', ''))) = 38;

ALTER TABLE gm_subsurveys
    CHANGE `surveyid` `surveyId` int(10) unsigned NOT NULL AUTO_INCREMENT,
    CHANGE `subsurveyid` `subsurveyId` int(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE gm_subsurveys
    DROP PRIMARY KEY,
    ADD PRIMARY KEY(`surveyId`,`subsurveyId`);

ALTER TABLE gm_surveys
    CHANGE `surveyid` `surveyId` int(10) unsigned NOT NULL AUTO_INCREMENT,
    CHANGE `player` `guid` int(10) unsigned NOT NULL DEFAULT '0',
    CHANGE `overall_comment` `overallComment` longtext NOT NULL,
    CHANGE `timestamp` `createTime` int(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE gm_surveys
    DROP PRIMARY KEY,
    ADD PRIMARY KEY (`surveyId`);

ALTER TABLE gm_tickets
    CHANGE `guid` `ticketId` int(10) unsigned NOT NULL AUTO_INCREMENT,
    CHANGE `playerGuid` `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier of ticket creator',
    CHANGE `name` `name` varchar(12) NOT NULL COMMENT 'Name of ticket creator',
    CHANGE `createtime` `createTime` int(10) unsigned NOT NULL DEFAULT '0',
    CHANGE `map` `mapId` smallint(5) unsigned NOT NULL DEFAULT '0',
    CHANGE `timestamp` `lastModifiedTime` int(10) unsigned NOT NULL DEFAULT '0',
    CHANGE `closed` `closedBy` int(10) NOT NULL DEFAULT '0',
    CHANGE `assignedto` `assignedTo` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'GUID of admin to whom ticket is assigned';

ALTER TABLE gm_tickets
    DROP PRIMARY KEY,
    ADD PRIMARY KEY (`ticketId`);

ALTER TABLE lag_reports
    CHANGE `report_id` `reportId` int(10) unsigned NOT NULL AUTO_INCREMENT,
    CHANGE `player` `guid` int(10) unsigned NOT NULL DEFAULT '0',
    CHANGE `lag_type` `lagType` tinyint(3) unsigned NOT NULL DEFAULT '0',
    CHANGE `map` `mapId` smallint(5) unsigned NOT NULL DEFAULT '0';

ALTER TABLE lag_reports
    DROP PRIMARY KEY,
    ADD PRIMARY KEY (`reportId`);
ALTER TABLE `corpse` CHANGE COLUMN `phaseMask` `phaseMask` smallint(5) unsigned NOT NULL DEFAULT '1';
ALTER TABLE `character_banned` CHANGE COLUMN `guid` `guid` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier';
ALTER TABLE `guild_rank` CHANGE `rname` `rname` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' NOT NULL;
ALTER TABLE `character_stats` ADD `resilience` INT UNSIGNED NOT NULL DEFAULT 0;
-- Uncomment this query if the ALTER query gives you primary key violation errors
-- SET @item_guid := (SELECT `item_guid` FROM `mail_items` GROUP BY `item_guid` HAVING COUNT(`item_guid`) > 1);
-- DELETE FROM `mail_items` WHERE `item_guid` IN (@item_guid);
ALTER TABLE `mail_items`
DROP PRIMARY KEY,
ADD PRIMARY KEY(`item_guid`),
ADD KEY `idx_mail_id` (`mail_id`);
ALTER TABLE `character_pet`
ADD KEY `idx_slot` (`slot`);
ALTER TABLE `character_pet`
DROP COLUMN `resettalents_cost`,
DROP COLUMN `resettalents_time`;
ALTER TABLE `characters` ADD COLUMN `grantableLevels` tinyint(3) unsigned NOT NULL DEFAULT '0' AFTER `actionBars`;
ALTER TABLE `character_queststatus` ADD `playercount` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0';
-- Replaces MATRON with PATRON title on MALE char.
UPDATE characters SET knownTitles = CONCAT(
SUBSTRING_INDEX(SUBSTRING_INDEX(knownTitles, ' ', 1), ' ', -1), ' ',
SUBSTRING_INDEX(SUBSTRING_INDEX(knownTitles, ' ', 2), ' ', -1), ' ',
SUBSTRING_INDEX(SUBSTRING_INDEX(knownTitles, ' ', 3), ' ', -1), ' ',
(SUBSTRING_INDEX(SUBSTRING_INDEX(knownTitles, ' ', 4), ' ', -1) | 512) &~256, ' ',
SUBSTRING_INDEX(SUBSTRING_INDEX(knownTitles, ' ', 5), ' ', -1), ' ',
SUBSTRING_INDEX(SUBSTRING_INDEX(knownTitles, ' ', 6), ' ', -1))
WHERE SUBSTRING_INDEX(SUBSTRING_INDEX(knownTitles, ' ', 4), ' ', -1) & 256 AND gender = 0;
DROP TABLE IF EXISTS `lfg_data`;
CREATE TABLE `lfg_data` (
  `guid` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier',
  `dungeon` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `state` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='LFG Data';

DROP TABLE IF EXISTS `character_queststatus_seasonal`;
CREATE TABLE `character_queststatus_seasonal` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier',
  `quest` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quest Identifier',
  `event` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Event Identifier',
  PRIMARY KEY (`guid`,`quest`),
  KEY `idx_guid` (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Player System';
DELETE FROM character_queststatus WHERE `status` = 0;

DROP TABLE IF EXISTS `character_queststatus_rewarded`;
CREATE TABLE `character_queststatus_rewarded` (
  `guid` int(10) unsigned NOT NULL default '0' COMMENT 'Global Unique Identifier',
  `quest` int(10) unsigned NOT NULL default '0' COMMENT 'Quest Identifier',
  PRIMARY KEY (`guid`,`quest`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Player System';

INSERT INTO character_queststatus_rewarded SELECT guid, quest FROM character_queststatus WHERE rewarded = 1;
DELETE FROM character_queststatus WHERE rewarded = 1;

ALTER TABLE character_queststatus DROP COLUMN rewarded;

DELETE FROM `character_queststatus_rewarded` WHERE `quest` IN (8619,8635,8636,8642,8643,8644,8645,8646,8647,8648,8649,8650,8651,8652,8653,8654,8670,8671,8672,8673,8674,8675,8676,8677,8678,8679,8680,8681,8682,8683,8684,8685,8686,8688,8713,8714,8715,8716,8717,8718,8719,8720,8721,8722,8723,8724,8725,8726,8727,8862,8863,8864,8865,8866,8867,8870,8871,8872,8873,8874,8875,8876,8877,8878,8879,8880,8881,8882,8883,13012,13013,13014,13015,13016,13017,13018,13019,13020,13021,13022,13023,13024,13025,13026,13027,13028,13029,13030,13031,13032,13033,13065,13066,13067,11120,11431,11442,11409,11318,11117,11400,11419,11118,11122,11293,11294,11321,11407,11408,11412,11413,11441,11446,11454,11486,11487,12020,12022,12318,12193,12062,12491,11447,12191,12492,12194,12192,12278,12306,12420,12421,13932,13649,13931,11127,11320,11347,11437,11438,11444,11445,9319,9322,9323,9324,9325,9326,9330,9331,9332,9339,9365,9367,9368,9386,9388,9389,11657,11691,11731,11882,11883,11886,11891,11915,11917,11921,11922,11923,11924,11925,11926,11933,11935,11947,11948,11952,11953,11954,11955,11964,11966,11970,11971,11972,12012,14022,114023,114024,114028,114030,114033,114036,114037,114040,114041,114043,114044,114064,114065,114035,114047,114048,114051,114053,114054,114055,114058,114059,114060,114061,114062,114488,24597,24609,24656,24657,24745,24848,24849,24666,24792,24804,24611,24536,24612,24635,24636,24655,24610,24629,24614,24576,24613,24615,24638,24645,24647,24648,24649,24650,24651,24652,24658,24659,24660,24662,24663,24664,24665,24793,24805,24850,24851,24541,14483,24661,1657,1658,6961,6962,6963,6964,6983,6984,7021,7022,7023,7024,7025,7042,7043,7045,7061,7062,7063,8149,8150,8311,8312,8322,8353,8354,8355,8356,8357,8358,8359,8360,8373,8409,8744,8746,8762,8763,8767,8768,8769,8788,8799,8803,8827,8828,8860,8861,8868,8897,8898,8899,8900,8901,8902,8903,8904,8971,8972,8973,8974,8975,8976,8979,8980,8981,8982,8983,8984,8993,9024,9025,9026,9027,9028,11131,11135,11219,11220,11242,11356,11357,11360,11361,11392,11401,11403,11404,11405,11435,11439,11440,11449,11450,11528,11558,11580,11581,11583,11584,11696,11732,11734,11735,11736,11737,11738,11739,11740,11741,11742,11743,11744,11745,11746,11747,11748,11749,11750,11751,11752,11753,11754,11755,11756,11757,11758,11759,11760,11761,11762,11763,11764,11765,11766,11767,11768,11769,11770,11771,11772,11773,11774,11775,11776,11777,11778,11779,11780,11781,11782,11783,11784,11785,11786,11787,11799,11800,11801,11802,11803,11804,11805,11806,11807,11808,11809,11810,11811,11812,11813,11814,11815,11816,11817,11818,11819,11820,11821,11822,11823,11824,11825,11826,11827,11828,11829,11830,11831,11832,11833,11834,11835,11836,11837,11838,11839,11840,11841,11842,11843,11844,11845,11846,11847,11848,11849,11850,11851,11852,11853,11854,11855,11856,11857,11858,11859,11860,11861,11862,11863,11937,11976,12133,12135,12139,12155,12286,12313,12331,12332,12333,12334,12335,12336,12337,12338,12339,12340,12341,12342,12343,12344,12345,12346,12347,12348,12349,12350,12351,12352,12353,12354,12355,12356,12357,12358,12359,12360,12361,12362,12363,12364,12365,12366,12367,12368,12369,12370,12371,12373,12374,12375,12376,12377,12378,12379,12380,12381,12382,12383,12384,12385,12386,12387,12388,12389,12390,12391,12392,12393,12394,12395,12396,12397,12398,12399,12400,12401,12402,12403,12404,12405,12406,12407,12408,12409,12410,12940,12941,12944,12945,12946,12947,12950,13203,13433,13434,13435,13436,13437,13438,13439,13440,13441,13442,13443,13444,13445,13446,13447,13448,13449,13450,13451,13452,13453,13454,13455,13456,13457,13458,13459,13460,13461,13462,13463,13464,13465,13466,13467,13468,13469,13470,13471,13472,13473,13474,13485,13486,13487,13488,13489,13490,13491,13492,13493,13494,13495,13496,13497,13498,13499,13500,13501,13548,13966);
alter table `account_data` change `account` `accountId` int(10) unsigned NOT NULL DEFAULT '0';/*
Navicat MySQL Data Transfer

Source Server         : Trinity
Source Server Version : 50158
Source Host           : 192.168.1.101:3306
Source Database       : daemon_char

Target Server Type    : MYSQL
Target Server Version : 50158
File Encoding         : 65001

Date: 2012-02-11 18:54:57
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `account_tutorial`
-- ----------------------------
DROP TABLE IF EXISTS `account_tutorial`;
CREATE TABLE `account_tutorial` (
  `accountId` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Account Identifier',
  `tut0` int(10) unsigned NOT NULL DEFAULT '0',
  `tut1` int(10) unsigned NOT NULL DEFAULT '0',
  `tut2` int(10) unsigned NOT NULL DEFAULT '0',
  `tut3` int(10) unsigned NOT NULL DEFAULT '0',
  `tut4` int(10) unsigned NOT NULL DEFAULT '0',
  `tut5` int(10) unsigned NOT NULL DEFAULT '0',
  `tut6` int(10) unsigned NOT NULL DEFAULT '0',
  `tut7` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`accountId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of account_tutorial
-- ----------------------------
ALTER TABLE `character_aura`  ADD  `item_guid` bigint(20) unsigned NOT NULL DEFAULT '0' AFTER `caster_guid`;
ALTER TABLE `item_instance` ADD COLUMN `itemEntry` mediumint(8) unsigned NOT NULL DEFAULT '0' AFTER `guid`;

-- Set values for new column from corresponding columns in other tables
UPDATE item_instance ii, auctionhouse ah
SET ii.itemEntry = ah.item_template
WHERE ii.guid = ah.itemguid;

UPDATE item_instance ii, character_inventory ci
SET ii.itemEntry = ci.item_template
WHERE ii.guid = ci.item;

UPDATE item_instance ii, guild_bank_item gbi
SET ii.itemEntry = gbi.item_entry
WHERE ii.guid = gbi.item_guid;

UPDATE item_instance ii, mail_items mi
SET ii.itemEntry = mi.item_template
WHERE ii.guid = mi.item_guid;

-- Remove unnecessary columns
ALTER TABLE `auctionhouse` DROP COLUMN `item_template`;
ALTER TABLE `character_inventory` DROP COLUMN `item_template`;
ALTER TABLE `guild_bank_item` DROP COLUMN `item_entry`;
ALTER TABLE `mail_items` DROP COLUMN `item_template`;


ALTER TABLE `character_aura`
  ADD COLUMN `item_guid`  bigint(20) UNSIGNED NOT NULL DEFAULT '0' AFTER `caster_guid`,
  DROP PRIMARY KEY,
  ADD PRIMARY KEY (`guid`,`caster_guid`,`item_guid`,`spell`,`effect_mask`);
  
ALTER TABLE `creature_respawn` CHANGE COLUMN `respawntime` `respawntime` bigint(20) unsigned NOT NULL DEFAULT 0;


ALTER TABLE `gameobject_respawn` CHANGE COLUMN `respawntime` `respawntime` bigint(20) unsigned NOT NULL DEFAULT 0;


DROP TABLE IF EXISTS `character_queststatus_rewarded`;
CREATE TABLE `character_queststatus_rewarded` (
  `guid` int(10) unsigned NOT NULL default '0' COMMENT 'Global Unique Identifier',
  `quest` int(10) unsigned NOT NULL default '0' COMMENT 'Quest Identifier',
  PRIMARY KEY (`guid`,`quest`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Player System';

INSERT INTO character_queststatus_rewarded SELECT guid, quest FROM character_queststatus WHERE rewarded = 1;
DELETE FROM character_queststatus WHERE rewarded = 1;

ALTER TABLE character_queststatus DROP COLUMN rewarded;


ALTER TABLE `account_data` 
CHANGE `account` `account` INT(10) UNSIGNED DEFAULT '0' NOT NULL, 
CHANGE `type` `type` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL, 
CHANGE `time` `time` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `data` `data` BLOB NOT NULL;



ALTER TABLE `addons` 
ROW_FORMAT=DEFAULT,
CHANGE `crc` `crc` INT(10) UNSIGNED DEFAULT '0' NOT NULL;ALTER TABLE `arena_team` 
CHANGE `name` `name` VARCHAR(24) NOT NULL,
CHANGE `EmblemStyle` `EmblemStyle` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `BorderStyle` `BorderStyle` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `auctionhouse`
CHANGE `id` `id` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `auctioneerguid` `auctioneerguid` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `itemguid` `itemguid` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `itemowner` `itemowner` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `buyoutprice` `buyoutprice` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `time` `time` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `buyguid` `buyguid` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `lastbid` `lastbid` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `startbid` `startbid` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `deposit` `deposit` INT(10) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `bugreport`
ROW_FORMAT=DEFAULT,
CHANGE `id` `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Identifier';


ALTER TABLE `channels`
ROW_FORMAT=DEFAULT,
CHANGE `BannedList` `BannedList` TEXT,
DROP PRIMARY KEY, 
ADD PRIMARY KEY (`m_name`, `m_team`);


ALTER TABLE `characters`
ROW_FORMAT=DEFAULT,
CHANGE `guid` `guid` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Global Unique Identifier',
CHANGE `account` `account` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Account Identifier',
CHANGE `map` `map` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Map Identifier',
CHANGE `instance_id` `instance_id` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `instance_mode_mask` `instance_mode_mask` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `taximask` `taximask` TEXT NOT NULL,
CHANGE `totaltime` `totaltime` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `leveltime` `leveltime` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `logout_time` `logout_time` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `resettalents_cost` `resettalents_cost` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `resettalents_time` `resettalents_time` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `transguid` `transguid` MEDIUMINT(8) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `extra_flags` `extra_flags` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `stable_slots` `stable_slots` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `at_login` `at_login` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `zone` `zone` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `death_expire_time` `death_expire_time` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `watchedFaction` `watchedFaction` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `latency` `latency` MEDIUMINT(8) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `deleteInfos_Account` `deleteInfos_Account` INT(10) UNSIGNED NULL ,
CHANGE `deleteDate` `deleteDate` INT(10) UNSIGNED NULL ;


ALTER TABLE `character_account_data`
CHANGE `guid` `guid` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `type` `type` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `time` `time` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `data` `data` BLOB NOT NULL;


ALTER TABLE `character_achievement`
CHANGE `guid` `guid` INT(10) UNSIGNED NOT NULL,
CHANGE `achievement` `achievement` SMALLINT(5) UNSIGNED NOT NULL,
CHANGE `date` `date` INT(10) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `character_achievement_progress`
CHANGE `guid` `guid` INT(10) UNSIGNED NOT NULL,
CHANGE `criteria` `criteria` SMALLINT(5) UNSIGNED NOT NULL,
CHANGE `counter` `counter` INT(10) UNSIGNED NOT NULL,
CHANGE `date` `date` INT(10) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `character_action`
ROW_FORMAT=DEFAULT,
CHANGE `guid` `guid` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `action` `action` INT(10) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `character_arena_stats`
ENGINE=InnoDB,
CHANGE `slot` `slot` TINYINT(3) NOT NULL;


ALTER TABLE `character_aura`
ROW_FORMAT=DEFAULT,
CHANGE `guid` `guid` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Global Unique Identifier',
CHANGE `spell` `spell` MEDIUMINT(8) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `character_banned`
ROW_FORMAT=DEFAULT,
CHANGE `guid` `guid` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Account id',
CHANGE `bandate` `bandate` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `unbandate` `unbandate` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `active` `active` TINYINT(3) UNSIGNED DEFAULT '1' NOT NULL;


ALTER TABLE `character_battleground_data`
ROW_FORMAT=DEFAULT,
CHANGE `guid` `guid` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Global Unique Identifier',
CHANGE `instance_id` `instance_id` INT(10) UNSIGNED NOT NULL, 
CHANGE `team` `team` SMALLINT(5) UNSIGNED NOT NULL,
CHANGE `join_map` `join_map` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `taxi_start` `taxi_start` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `taxi_end` `taxi_end` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `mount_spell` `mount_spell` MEDIUMINT(8) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `character_battleground_random`
CHANGE `guid` `guid` INT(10) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `character_declinedname`
ROW_FORMAT=DEFAULT,
CHANGE `guid` `guid` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Global Unique Identifier';


ALTER TABLE `character_equipmentsets`
CHANGE `guid` `guid` INT(10) DEFAULT '0' NOT NULL,
CHANGE `setindex` `setindex` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `name` `name` VARCHAR(31) NOT NULL,
CHANGE `item0` `item0` INT(10) UNSIGNED NOT NULL DEFAULT '0', 
CHANGE `item1` `item1` INT(10) UNSIGNED NOT NULL DEFAULT '0', 
CHANGE `item2` `item2` INT(10) UNSIGNED NOT NULL DEFAULT '0', 
CHANGE `item3` `item3` INT(10) UNSIGNED NOT NULL DEFAULT '0', 
CHANGE `item4` `item4` INT(10) UNSIGNED NOT NULL DEFAULT '0', 
CHANGE `item5` `item5` INT(10) UNSIGNED NOT NULL DEFAULT '0', 
CHANGE `item6` `item6` INT(10) UNSIGNED NOT NULL DEFAULT '0', 
CHANGE `item7` `item7` INT(10) UNSIGNED NOT NULL DEFAULT '0', 
CHANGE `item8` `item8` INT(10) UNSIGNED NOT NULL DEFAULT '0', 
CHANGE `item9` `item9` INT(10) UNSIGNED NOT NULL DEFAULT '0', 
CHANGE `item10` `item10` INT(10) UNSIGNED NOT NULL DEFAULT '0', 
CHANGE `item11` `item11` INT(10) UNSIGNED NOT NULL DEFAULT '0', 
CHANGE `item12` `item12` INT(10) UNSIGNED NOT NULL DEFAULT '0', 
CHANGE `item13` `item13` INT(10) UNSIGNED NOT NULL DEFAULT '0', 
CHANGE `item14` `item14` INT(10) UNSIGNED NOT NULL DEFAULT '0', 
CHANGE `item15` `item15` INT(10) UNSIGNED NOT NULL DEFAULT '0', 
CHANGE `item16` `item16` INT(10) UNSIGNED NOT NULL DEFAULT '0', 
CHANGE `item17` `item17` INT(10) UNSIGNED NOT NULL DEFAULT '0', 
CHANGE `item18` `item18` INT(10) UNSIGNED NOT NULL DEFAULT '0';


ALTER TABLE `character_gifts`
CHANGE `guid` `guid` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `item_guid` `item_guid` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `entry` `entry` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `flags` `flags` INT(10) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `character_glyphs`
CHANGE `guid` `guid` INT(10) UNSIGNED NOT NULL,
CHANGE `glyph1` `glyph1` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `glyph2` `glyph2` SMALLINT(5) UNSIGNED DEFAULT '0' NULL ,
CHANGE `glyph3` `glyph3` SMALLINT(5) UNSIGNED DEFAULT '0' NULL ,
CHANGE `glyph4` `glyph4` SMALLINT(5) UNSIGNED DEFAULT '0' NULL ,
CHANGE `glyph5` `glyph5` SMALLINT(5) UNSIGNED DEFAULT '0' NULL ,
CHANGE `glyph6` `glyph6` SMALLINT(5) UNSIGNED DEFAULT '0' NULL ;


ALTER TABLE `character_homebind`
ROW_FORMAT=DEFAULT,
CHANGE `guid` `guid` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Global Unique Identifier',
CHANGE `map` `map` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Map Identifier',
CHANGE `zone` `zone` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Zone Identifier';


ALTER TABLE `character_instance`
CHANGE `guid` `guid` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `instance` `instance` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `permanent` `permanent` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `character_inventory`
ROW_FORMAT=DEFAULT,
CHANGE `guid` `guid` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Global Unique Identifier',
CHANGE `bag` `bag` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `item` `item` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Item Global Unique Identifier';


ALTER TABLE `character_pet`
ROW_FORMAT=DEFAULT,
CHANGE `id` `id` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `entry` `entry` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `owner` `owner` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `modelid` `modelid` INT(10) UNSIGNED DEFAULT '0' NULL ,
CHANGE `CreatedBySpell` `CreatedBySpell` MEDIUMINT(8) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `level` `level` SMALLINT(5) UNSIGNED DEFAULT '1' NOT NULL,
CHANGE `exp` `exp` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `Reactstate` `Reactstate` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `name` `name` VARCHAR(21) DEFAULT 'Pet' NOT NULL,
CHANGE `renamed` `renamed` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `slot` `slot` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `curhealth` `curhealth` INT(10) UNSIGNED DEFAULT '1' NOT NULL,
CHANGE `curmana` `curmana` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `curhappiness` `curhappiness` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `savetime` `savetime` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `resettalents_cost` `resettalents_cost` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `resettalents_time` `resettalents_time` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `abdata` `abdata` TEXT NULL;


ALTER TABLE `character_pet_declinedname`
ROW_FORMAT=DEFAULT,
CHANGE `id` `id` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `owner` `owner` INT(10) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `character_queststatus`
ROW_FORMAT=DEFAULT,
CHANGE `guid` `guid` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Global Unique Identifier',
CHANGE `quest` `quest` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Quest Identifier',
CHANGE `status` `status` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `explored` `explored` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `timer` `timer` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `mobcount1` `mobcount1` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `mobcount2` `mobcount2` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `mobcount3` `mobcount3` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `mobcount4` `mobcount4` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `itemcount1` `itemcount1` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `itemcount2` `itemcount2` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `itemcount3` `itemcount3` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `itemcount4` `itemcount4` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `character_queststatus_daily`
ROW_FORMAT=DEFAULT,
CHANGE `guid` `guid` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Global Unique Identifier',
CHANGE `quest` `quest` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Quest Identifier',
CHANGE `time` `time` INT(10) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `character_queststatus_weekly`
ROW_FORMAT=DEFAULT,
CHANGE `guid` `guid` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Global Unique Identifier',
CHANGE `quest` `quest` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Quest Identifier';


ALTER TABLE `character_reputation`
ROW_FORMAT=DEFAULT,
CHANGE `guid` `guid` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Global Unique Identifier',
CHANGE `faction` `faction` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `flags` `flags` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `character_skills`
ROW_FORMAT=DEFAULT,
CHANGE `guid` `guid` INT(10) UNSIGNED NOT NULL COMMENT 'Global Unique Identifier',
CHANGE `skill` `skill` SMALLINT(5) UNSIGNED NOT NULL,
CHANGE `value` `value` SMALLINT(5) UNSIGNED NOT NULL,
CHANGE `max` `max` SMALLINT(5) UNSIGNED NOT NULL;


ALTER TABLE `character_social`
ROW_FORMAT=DEFAULT,
CHANGE `guid` `guid` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Character Global Unique Identifier',
CHANGE `friend` `friend` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Friend Global Unique Identifier',
CHANGE `flags` `flags` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Friend Flags',
DROP INDEX `guid`,
DROP INDEX `guid_flags`,
DROP INDEX `friend_flags`;


ALTER TABLE `character_spell`
ROW_FORMAT=DEFAULT,
CHANGE `guid` `guid` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Global Unique Identifier',
CHANGE `spell` `spell` MEDIUMINT(8) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Spell Identifier';


ALTER TABLE `character_spell_cooldown`
CHANGE `guid` `guid` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Global Unique Identifier, Low part',
CHANGE `spell` `spell` MEDIUMINT(8) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Spell Identifier',
CHANGE `item` `item` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Item Identifier',
CHANGE `time` `time` INT(10) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `character_stats`
CHANGE `guid` `guid` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Global Unique Identifier, Low part';


ALTER TABLE `character_talent`
CHANGE `guid` `guid` INT(10) UNSIGNED NOT NULL,
CHANGE `spell` `spell` MEDIUMINT(8) UNSIGNED NOT NULL;


ALTER TABLE `character_tutorial`
ROW_FORMAT=DEFAULT,
CHANGE `account` `account` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Account Identifier',
DROP PRIMARY KEY, 
ADD PRIMARY KEY (`account`),
DROP `realmid`,
CHANGE `tut0` `tut0` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `tut1` `tut1` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `tut2` `tut2` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `tut3` `tut3` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `tut4` `tut4` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `tut5` `tut5` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `tut6` `tut6` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `tut7` `tut7` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
DROP KEY `acc_key`;


ALTER TABLE `corpse`
ROW_FORMAT=DEFAULT,
CHANGE `guid` `guid` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Global Unique Identifier',
CHANGE `player` `player` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Character Global Unique Identifier',
DROP COLUMN `zone`,
CHANGE `map` `map` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Map Identifier',
CHANGE `phaseMask` `phaseMask` TINYINT(3) UNSIGNED DEFAULT '1' NOT NULL,
CHANGE `flags` `flags` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `dynFlags` `dynFlags` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `time` `time` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `instance` `instance` INT(10) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `creature_respawn`
CHANGE `respawntime` `respawntime` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `instance` `instance` INT(10) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `game_event_condition_save`
CHANGE `event_id` `event_id` SMALLINT(5) UNSIGNED NOT NULL,
CHANGE `condition_id` `condition_id` INT(10) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `gameobject_respawn`
CHANGE `respawntime` `respawntime` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `instance` `instance` INT(10) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `game_event_save`
CHANGE `event_id` `event_id` SMALLINT(5) UNSIGNED NOT NULL,
CHANGE `next_start` `next_start` INT(10) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `gm_subsurveys`
ROW_FORMAT=DEFAULT,
CHANGE `surveyid` `surveyid` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
CHANGE `subsurveyid` `subsurveyid` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `rank` `rank` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `comment` `comment` TEXT NOT NULL;


ALTER TABLE `gm_surveys`
ROW_FORMAT=DEFAULT,
CHANGE `surveyid` `surveyid` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
CHANGE `player` `player` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `mainSurvey` `mainSurvey` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `timestamp` `timestamp` INT(10) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `gm_tickets`
ROW_FORMAT=DEFAULT,
CHANGE `guid` `guid` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
CHANGE `playerGuid` `playerGuid` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `name` `name` VARCHAR(12) NOT NULL,
CHANGE `createtime` `createtime` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `map` `map` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `timestamp` `timestamp` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `closed` `closed` INT(11) DEFAULT '0' NOT NULL,
CHANGE `assignedto` `assignedto` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `completed` `completed` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `escalated` `escalated` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `viewed` `viewed` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `groups`
ROW_FORMAT=DEFAULT,
CHANGE `guid` `guid` INT(10) UNSIGNED NOT NULL,
CHANGE `leaderGuid` `leaderGuid` INT(10) UNSIGNED NOT NULL,
CHANGE `lootMethod` `lootMethod` TINYINT(3) UNSIGNED NOT NULL,
CHANGE `looterGuid` `looterGuid` INT(10) UNSIGNED NOT NULL,
CHANGE `lootThreshold` `lootThreshold` TINYINT(3) UNSIGNED NOT NULL,
CHANGE `icon1` `icon1` INT(10) UNSIGNED NOT NULL,
CHANGE `icon2` `icon2` INT(10) UNSIGNED NOT NULL,
CHANGE `icon3` `icon3` INT(10) UNSIGNED NOT NULL,
CHANGE `icon4` `icon4` INT(10) UNSIGNED NOT NULL,
CHANGE `icon5` `icon5` INT(10) UNSIGNED NOT NULL,
CHANGE `icon6` `icon6` INT(10) UNSIGNED NOT NULL,
CHANGE `icon7` `icon7` INT(10) UNSIGNED NOT NULL,
CHANGE `icon8` `icon8` INT(10) UNSIGNED NOT NULL,
CHANGE `groupType` `groupType` TINYINT(3) UNSIGNED NOT NULL,
CHANGE `raiddifficulty` `raiddifficulty` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `group_instance`
CHANGE `guid` `guid` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `instance` `instance` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `permanent` `permanent` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `group_member`
ROW_FORMAT=DEFAULT,
CHANGE `guid` `guid` INT(10) UNSIGNED NOT NULL,
CHANGE `memberGuid` `memberGuid` INT(10) UNSIGNED NOT NULL;


ALTER TABLE `guild`
ROW_FORMAT=DEFAULT,
CHANGE `guildid` `guildid` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `name` `name` VARCHAR(24) DEFAULT '' NOT NULL,
CHANGE `leaderguid` `leaderguid` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `EmblemStyle` `EmblemStyle` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `EmblemColor` `EmblemColor` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `BorderStyle` `BorderStyle` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `BorderColor` `BorderColor` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `BackgroundColor` `BackgroundColor` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `motd` `motd` VARCHAR(128) DEFAULT '' NOT NULL,
CHANGE `createdate` `createdate` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `BankMoney` `BankMoney` BIGINT(20) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `guild_bank_eventlog`
CHANGE `guildid` `guildid` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Guild Identificator',
CHANGE `LogGuid` `LogGuid` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Log record identificator - auxiliary column',
CHANGE `PlayerGuid` `PlayerGuid` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `ItemOrMoney` `ItemOrMoney` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `ItemStackCount` `ItemStackCount` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0',
CHANGE `DestTabId` `DestTabId` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Destination Tab Id',
CHANGE `TimeStamp` `TimeStamp` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Event UNIX time';


ALTER TABLE `guild_bank_item`
CHANGE `guildid` `guildid` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `TabId` `TabId` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `item_guid` `item_guid` INT(10) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `guild_bank_right`
CHANGE `guildid` `guildid` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `TabId` `TabId` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `rid` `rid` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `SlotPerDay` `SlotPerDay` INT(10) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `guild_bank_tab`
CHANGE `guildid` `guildid` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `TabId` `TabId` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `TabName` `TabName` VARCHAR(16) DEFAULT '' NOT NULL,
CHANGE `TabText` `TabText` VARCHAR(500) NULL;


ALTER TABLE `guild_eventlog`
CHANGE `guildid` `guildid` INT(10) UNSIGNED NOT NULL COMMENT 'Guild Identificator',
CHANGE `LogGuid` `LogGuid` INT(10) UNSIGNED NOT NULL COMMENT 'Log record identificator - auxiliary column',
CHANGE `EventType` `EventType` TINYINT(3) UNSIGNED NOT NULL COMMENT 'Event type',
CHANGE `PlayerGuid1` `PlayerGuid1` INT(10) UNSIGNED NOT NULL COMMENT 'Player 1',
CHANGE `PlayerGuid2` `PlayerGuid2` INT(10) UNSIGNED NOT NULL COMMENT 'Player 2',
CHANGE `NewRank` `NewRank` TINYINT(3) UNSIGNED NOT NULL COMMENT 'New rank(in case promotion/demotion)',
CHANGE `TimeStamp` `TimeStamp` INT(10) UNSIGNED NOT NULL COMMENT 'Event UNIX time';


ALTER TABLE `guild_member`
ROW_FORMAT=DEFAULT,
CHANGE `guildid` `guildid` INT(10) UNSIGNED NOT NULL COMMENT 'Guild Identificator',
CHANGE `guid` `guid` INT(10) UNSIGNED NOT NULL,
CHANGE `rank` `rank` TINYINT(3) UNSIGNED NOT NULL,
CHANGE `pnote` `pnote` varchar(31) NOT NULL DEFAULT '',
CHANGE `offnote` `offnote` varchar(31) NOT NULL DEFAULT '',
CHANGE `BankResetTimeMoney` `BankResetTimeMoney` INT(10) UNSIGNED NOT NULL DEFAULT '0',
CHANGE `BankRemMoney` `BankRemMoney` INT(10) UNSIGNED NOT NULL DEFAULT '0',
CHANGE `BankResetTimeTab0` `BankResetTimeTab0` INT(10) UNSIGNED NOT NULL DEFAULT '0',
CHANGE `BankRemSlotsTab0` `BankRemSlotsTab0` INT(10) UNSIGNED NOT NULL DEFAULT '0',
CHANGE `BankResetTimeTab1` `BankResetTimeTab1` INT(10) UNSIGNED NOT NULL DEFAULT '0',
CHANGE `BankRemSlotsTab1` `BankRemSlotsTab1` INT(10) UNSIGNED NOT NULL DEFAULT '0',
CHANGE `BankResetTimeTab2` `BankResetTimeTab2` INT(10) UNSIGNED NOT NULL DEFAULT '0',
CHANGE `BankRemSlotsTab2` `BankRemSlotsTab2` INT(10) UNSIGNED NOT NULL DEFAULT '0',
CHANGE `BankResetTimeTab3` `BankResetTimeTab3` INT(10) UNSIGNED NOT NULL DEFAULT '0',
CHANGE `BankRemSlotsTab3` `BankRemSlotsTab3` INT(10) UNSIGNED NOT NULL DEFAULT '0',
CHANGE `BankResetTimeTab4` `BankResetTimeTab4` INT(10) UNSIGNED NOT NULL DEFAULT '0',
CHANGE `BankRemSlotsTab4` `BankRemSlotsTab4` INT(10) UNSIGNED NOT NULL DEFAULT '0',
CHANGE `BankResetTimeTab5` `BankResetTimeTab5` INT(10) UNSIGNED NOT NULL DEFAULT '0',
CHANGE `BankRemSlotsTab5` `BankRemSlotsTab5` INT(10) UNSIGNED NOT NULL DEFAULT '0';


ALTER TABLE `guild_rank`
ROW_FORMAT=DEFAULT,
CHANGE `guildid` `guildid` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `rid` `rid` TINYINT(3) UNSIGNED NOT NULL,
CHANGE `rname` `rname` VARCHAR(15) DEFAULT '' NOT NULL,
CHANGE `rights` `rights` MEDIUMINT(8) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `BankMoneyPerDay` `BankMoneyPerDay` INT(10) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `instance`
CHANGE `id` `id` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `map` `map` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `resettime` `resettime` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `difficulty` `difficulty` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `data` `data` TINYTEXT NOT NULL;


ALTER TABLE `instance_reset`
CHANGE `mapid` `mapid` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `difficulty` `difficulty` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `resettime` `resettime` INT(10) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `item_instance`
ROW_FORMAT=DEFAULT,
CHANGE `guid` `guid` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `owner_guid` `owner_guid` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `charges` `charges` TINYTEXT,
CHANGE `duration` `duration` INT(10) DEFAULT '0' NOT NULL,
CHANGE `flags` `flags` MEDIUMINT(8) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `randomPropertyId` `randomPropertyId` SMALLINT(5) DEFAULT '0' NOT NULL,
CHANGE `durability` `durability` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `text` `text` TEXT NULL;



ALTER TABLE `item_refund_instance`
ROW_FORMAT=DEFAULT,
CHANGE `item_guid` `item_guid` INT(10) UNSIGNED NOT NULL COMMENT 'Item GUID',
CHANGE `player_guid` `player_guid` INT(10) UNSIGNED NOT NULL COMMENT 'Player GUID',
CHANGE `paidMoney` `paidMoney` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `paidExtendedCost` `paidExtendedCost` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `item_soulbound_trade_data`
ROW_FORMAT=DEFAULT,
CHANGE `itemGuid` `itemGuid` INT(10) UNSIGNED NOT NULL COMMENT 'Item GUID';


ALTER TABLE `lag_reports`
ROW_FORMAT=DEFAULT,
CHANGE `report_id` `report_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
CHANGE `player` `player` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `lag_type` `lag_type` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `map` `map` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `mail`
ROW_FORMAT=DEFAULT,
CHANGE `id` `id` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Identifier',
CHANGE `mailTemplateId` `mailTemplateId` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `sender` `sender` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Character Global Unique Identifier',
CHANGE `receiver` `receiver` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Character Global Unique Identifier',
CHANGE `expire_time` `expire_time` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `deliver_time` `deliver_time` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `money` `money` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `cod` `cod` INT(10) UNSIGNED DEFAULT '0' NOT NULL;




ALTER TABLE `mail_items`
CHANGE `mail_id` `mail_id` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `item_guid` `item_guid` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `receiver` `receiver` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Character Global Unique Identifier';


ALTER TABLE `petition`
ROW_FORMAT=DEFAULT,
CHANGE `name` `name` VARCHAR(24) NOT NULL,
CHANGE `type` `type` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `petition_sign`
ROW_FORMAT=DEFAULT,
CHANGE `petitionguid` `petitionguid` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `playerguid` `playerguid` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `player_account` `player_account` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `type` `type` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `pet_aura`
ROW_FORMAT=DEFAULT,
CHANGE `guid` `guid` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Global Unique Identifier',
CHANGE `spell` `spell` MEDIUMINT(8) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `amount0` `amount0` MEDIUMINT(8) NOT NULL, 
CHANGE `amount1` `amount1` MEDIUMINT(8) NOT NULL, 
CHANGE `amount2` `amount2` MEDIUMINT(8) NOT NULL, 
CHANGE `base_amount0` `base_amount0` MEDIUMINT(8) NOT NULL, 
CHANGE `base_amount1` `base_amount1` MEDIUMINT(8) NOT NULL, 
CHANGE `base_amount2` `base_amount2` MEDIUMINT(8) NOT NULL;


ALTER TABLE `pet_spell`
ROW_FORMAT=DEFAULT,
CHANGE `guid` `guid` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Global Unique Identifier',
CHANGE `spell` `spell` MEDIUMINT(8) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Spell Identifier',
CHANGE `active` `active` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `pet_spell_cooldown`
CHANGE `guid` `guid` INT(10) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Global Unique Identifier, Low part',
CHANGE `spell` `spell` MEDIUMINT(8) UNSIGNED DEFAULT '0' NOT NULL COMMENT 'Spell Identifier',
CHANGE `time` `time` INT(10) UNSIGNED DEFAULT '0' NOT NULL;


ALTER TABLE `worldstates`
ROW_FORMAT=DEFAULT,
CHANGE `entry` `entry` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `value` `value` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `comment` `comment` TINYTEXT;



ALTER TABLE `character_inventory` 
ADD UNIQUE KEY (`guid`,`bag`,`slot`);



--
-- Table structure for table `account_instance_times`
--
DROP TABLE IF EXISTS `account_instance_times`;



CREATE TABLE `account_instance_times` (
  `accountId` int(10) unsigned NOT NULL,
  `instanceId` int(10) unsigned NOT NULL DEFAULT '0',
  `releaseTime` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`accountId`,`instanceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



ALTER TABLE `instance` ADD COLUMN `completedEncounters` int(10) unsigned NOT NULL DEFAULT '0' AFTER `difficulty`;



ALTER TABLE `account_data` CHANGE COLUMN `account` `accountId` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Account Identifier';



RENAME TABLE `character_tutorial` TO `account_tutorial`;



ALTER TABLE `account_tutorial` CHANGE COLUMN `account` `accountId` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Account Identifier';



ALTER TABLE `channels` CHANGE COLUMN `m_name` `name` varchar(128) NOT NULL,
  CHANGE COLUMN `m_team` `team` int(10) unsigned NOT NULL,
  CHANGE COLUMN `m_announce` `announce` tinyint(3) unsigned NOT NULL DEFAULT '1',
  CHANGE COLUMN `m_ownership` `ownership` tinyint(3) unsigned NOT NULL DEFAULT '1',
  CHANGE COLUMN `m_password` `password` varchar(32) DEFAULT NULL,
  CHANGE COLUMN `BannedList` `bannedList` text,
  CHANGE COLUMN `last_used` `lastUsed` int(10) unsigned NOT NULL;
ALTER TABLE `corpse` CHANGE COLUMN `phaseMask` `phaseMask` smallint(5) unsigned NOT NULL DEFAULT '1';
-- Create temporary table that holds the entry conversion data. (See corresponding world db update)
CREATE TABLE `game_event_temp` (
  `entryOld` TINYINT(3) UNSIGNED NOT NULL,
  `entryNew` TINYINT(3) UNSIGNED NOT NULL
) ENGINE=INNODB DEFAULT CHARSET=utf8;

-- Renumbered entry data
INSERT INTO `game_event_temp` (`entryOld`,`entryNew`) VALUES
(1,1),(2,2),(3,3),(4,4),(5,5),(6,6),(7,7),(8,8),(9,9),(10,10),(11,11),(12,12),(13,13),(14,14),(15,15),(16,16),(17,17),(18,18),(19,19),(20,20),(21,21),(22,22),
(23,23),(26,24),(27,25),(28,26),(29,27),(30,28),(31,29),(32,30),(33,31),(34,32),(35,33),(36,34),(37,35),(38,36),(39,37),(40,38),(41,39),(42,40),(43,41),(44,42),
(45,43),(46,44),(47,45),(48,46),(49,47),(50,48),(51,49),(52,50),(53,51),(54,52),(124,53),(125,54),(126,55),(127,56),(128,57),(129,58),(130,59),(131,60);

-- Update game_event_save
UPDATE `game_event_save`, `game_event_temp` SET `game_event_save`.`event_id` = `game_event_temp`.`entryNew` WHERE `game_event_save`.`event_id` = `game_event_temp`.`entryOld`;
ALTER TABLE `game_event_save` CHANGE `event_id` `eventEntry` TINYINT(3) UNSIGNED NOT NULL;

-- Update game_event_condition_save
UPDATE `game_event_condition_save`, `game_event_temp` SET `game_event_condition_save`.`event_id` = `game_event_temp`.`entryNew` WHERE `game_event_condition_save`.`event_id` = `game_event_temp`.`entryOld`;
ALTER TABLE `game_event_condition_save` CHANGE `event_id` `eventEntry` TINYINT(3) UNSIGNED NOT NULL;
-- Remove temporary table
DROP TABLE `game_event_temp`;-- Update arena_team table
ALTER TABLE `arena_team`
CHANGE `arenateamid` `arenaTeamId` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `captainguid` `captainGuid` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `BackgroundColor` `backgroundColor` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `EmblemStyle` `emblemStyle` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `EmblemColor` `emblemColor` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `BorderStyle` `borderStyle` TINYINT(3) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `BorderColor` `borderColor` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
ADD COLUMN `rating` SMALLINT(5) UNSIGNED NOT NULL AFTER `type`,
ADD COLUMN `seasonGames` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL AFTER `rating`,
ADD COLUMN `seasonWins` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL AFTER `seasonGames`,
ADD COLUMN `weekGames` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL AFTER `seasonWins`,
ADD COLUMN `weekWins` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL AFTER `weekGames`,
ADD COLUMN `rank` INT(10) UNSIGNED NOT NULL AFTER `weekWins`;

-- Move data from arena_team_stats to new columns in arena_team
UPDATE `arena_team` a, `arena_team_stats` s SET
`a`.`rating` = `s`.`rating`,
`a`.`seasonGames` = `s`.`played`,
`a`.`seasonWins` = `s`.`wins2`,
`a`.`weekGames` = `s`.`games`,
`a`.`weekWins` = `s`.`wins`,
`a`.`rank` = `s`.`rank`
WHERE `a`.`arenaTeamId` = `s`.`arenateamid`;

-- Remove arena_team_stats table
DROP TABLE `arena_team_stats`;

-- Update arena_team_member table
ALTER TABLE `arena_team_member`
CHANGE `arenateamid` `arenaTeamId` INT(10) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `played_week` `weekGames` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `wons_week` `weekWins` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `played_season` `seasonGames` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL,
CHANGE `wons_season` `seasonWins` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL,
ADD COLUMN `personalRating` SMALLINT(5) UNSIGNED DEFAULT '0' NOT NULL AFTER `seasonWins`;

-- Update character_arena_stats table
ALTER TABLE `character_arena_stats`
CHANGE `personal_rating` `personalRating` SMALLINT(5) NOT NULL,
CHANGE `matchmaker_rating` `matchMakerRating` SMALLINT(5) NOT NULL;
-- Update arena_team_member table
ALTER TABLE `arena_team_member`
DROP COLUMN `personalRating`;
ALTER TABLE `character_battleground_data`
    CHANGE `instance_id` `instanceId` int(10) unsigned NOT NULL COMMENT 'Instance Identifier',
    CHANGE `join_map` `joinMapId` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Map Identifier',
    CHANGE `join_x` `joinX` float NOT NULL DEFAULT '0',
    CHANGE `join_y` `joinY` float NOT NULL DEFAULT '0',
    CHANGE `join_z` `joinZ` float NOT NULL DEFAULT '0',
    CHANGE `join_o` `joinO` float NOT NULL DEFAULT '0',
    CHANGE `taxi_start` `taxiStart` int(10) unsigned NOT NULL DEFAULT '0',
    CHANGE `taxi_end` `taxiEnd` int(10) unsigned NOT NULL DEFAULT '0',
    CHANGE `mount_spell` `mountSpell` mediumint(8) unsigned NOT NULL DEFAULT '0';
ALTER TABLE `character_homebind`
    CHANGE `map` `mapId` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Map Identifier',
    CHANGE `zone` `zoneId` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Zone Identifier',
    CHANGE `position_x` `posX` float NOT NULL DEFAULT '0',
    CHANGE `position_y` `posY` float NOT NULL DEFAULT '0',
    CHANGE `position_z` `posZ` float NOT NULL DEFAULT '0';
ALTER TABLE `arena_team`
CHANGE `rating` `rating` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0',
CHANGE `rank` `rank` INT(10) UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `arena_team_member`
ADD COLUMN `personalRating` SMALLINT(5) NOT NULL DEFAULT '0' AFTER guid;

UPDATE arena_team_member
INNER JOIN character_arena_stats ON arena_team_member.guid = character_arena_stats.guid
INNER JOIN arena_team ON arena_team.arenaTeamId = arena_team_member.arenaTeamId AND (slot = 0 AND TYPE = 2 OR slot = 1 AND TYPE = 3 OR slot = 2 AND TYPE = 5)
SET arena_team_member.personalRating = character_arena_stats.personalRating;

ALTER TABLE `character_arena_stats`
DROP COLUMN `personalRating`;
ALTER TABLE `corpse`
    CHANGE `guid` `corpseGuid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier',
    CHANGE `player` `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Character Global Unique Identifier',
    CHANGE `map` `mapId` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Map Identifier',
    CHANGE `position_x` `posX` float NOT NULL DEFAULT '0',
    CHANGE `position_y` `posY` float NOT NULL DEFAULT '0',
    CHANGE `position_z` `posZ` float NOT NULL DEFAULT '0',
    CHANGE `guild` `guildId` int(10) unsigned NOT NULL DEFAULT '0',
    CHANGE `corpse_type` `corpseType` tinyint(3) unsigned NOT NULL DEFAULT '0',
    CHANGE `instance` `instanceId` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Instance Identifier';
ALTER TABLE `creature_respawn`
    CHANGE `respawntime` `respawnTime` int(10) unsigned NOT NULL DEFAULT '0',
    CHANGE `instance` `instanceId` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Instance Identifier';
ALTER TABLE `gameobject_respawn`
    CHANGE `respawntime` `respawnTime` int(10) unsigned NOT NULL DEFAULT '0',
    CHANGE `instance` `instanceId` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Instance Identifier';
ALTER TABLE corpse
    DROP PRIMARY KEY,
    DROP KEY `idx_type`,
    DROP KEY `instance`,
    DROP KEY `Idx_player`,
    DROP KEY `Idx_time`;

ALTER TABLE corpse
    ADD PRIMARY KEY (`corpseGuid`),
    ADD KEY `idx_type`(`corpseType`),
    ADD KEY `idx_instance`(`instanceId`),
    ADD KEY `idx_player`(`guid`),
    ADD KEY `idx_time`(`time`);

ALTER TABLE creature_respawn
    DROP PRIMARY KEY,
    DROP KEY `instance`;

ALTER TABLE creature_respawn
    ADD PRIMARY KEY (`guid`, `instanceId`),
    ADD KEY `idx_instance`(`instanceId`);

ALTER TABLE gameobject_respawn
    DROP PRIMARY KEY,
    DROP KEY `instance`;

ALTER TABLE gameobject_respawn
    ADD PRIMARY KEY (`guid`, `instanceId`),
    ADD KEY `idx_instance`(`instanceId`);
-- append extra zeros, only if the string is properly formatted (has 38 tokens)
UPDATE `characters` SET `equipmentCache`=CONCAT(`equipmentCache`,"0 0 0 0 0 0 0 0 ") WHERE (CHAR_LENGTH(`equipmentCache`) - CHAR_LENGTH(REPLACE(`equipmentCache`, ' ', ''))) = 38;
ALTER TABLE gm_subsurveys
    CHANGE `surveyid` `surveyId` int(10) unsigned NOT NULL AUTO_INCREMENT,
    CHANGE `subsurveyid` `subsurveyId` int(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE gm_subsurveys
    DROP PRIMARY KEY,
    ADD PRIMARY KEY(`surveyId`,`subsurveyId`);

ALTER TABLE gm_surveys
    CHANGE `surveyid` `surveyId` int(10) unsigned NOT NULL AUTO_INCREMENT,
    CHANGE `player` `guid` int(10) unsigned NOT NULL DEFAULT '0',
    CHANGE `overall_comment` `overallComment` longtext NOT NULL,
    CHANGE `timestamp` `createTime` int(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE gm_surveys
    DROP PRIMARY KEY,
    ADD PRIMARY KEY (`surveyId`);

ALTER TABLE gm_tickets
    CHANGE `guid` `ticketId` int(10) unsigned NOT NULL AUTO_INCREMENT,
    CHANGE `playerGuid` `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier of ticket creator',
    CHANGE `name` `name` varchar(12) NOT NULL COMMENT 'Name of ticket creator',
    CHANGE `createtime` `createTime` int(10) unsigned NOT NULL DEFAULT '0',
    CHANGE `map` `mapId` smallint(5) unsigned NOT NULL DEFAULT '0',
    CHANGE `timestamp` `lastModifiedTime` int(10) unsigned NOT NULL DEFAULT '0',
    CHANGE `closed` `closedBy` int(10) NOT NULL DEFAULT '0',
    CHANGE `assignedto` `assignedTo` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'GUID of admin to whom ticket is assigned';

ALTER TABLE gm_tickets
    DROP PRIMARY KEY,
    ADD PRIMARY KEY (`ticketId`);

ALTER TABLE lag_reports
    CHANGE `report_id` `reportId` int(10) unsigned NOT NULL AUTO_INCREMENT,
    CHANGE `player` `guid` int(10) unsigned NOT NULL DEFAULT '0',
    CHANGE `lag_type` `lagType` tinyint(3) unsigned NOT NULL DEFAULT '0',
    CHANGE `map` `mapId` smallint(5) unsigned NOT NULL DEFAULT '0';

ALTER TABLE lag_reports
    DROP PRIMARY KEY,
    ADD PRIMARY KEY (`reportId`);
ALTER TABLE `corpse` CHANGE COLUMN `phaseMask` `phaseMask` smallint(5) unsigned NOT NULL DEFAULT '1';
ALTER TABLE `character_banned` CHANGE COLUMN `guid` `guid` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier';
ALTER TABLE `guild_rank` CHANGE `rname` `rname` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' NOT NULL;
ALTER TABLE `character_stats` ADD `resilience` INT UNSIGNED NOT NULL DEFAULT 0;
-- Uncomment this query if the ALTER query gives you primary key violation errors
-- SET @item_guid := (SELECT `item_guid` FROM `mail_items` GROUP BY `item_guid` HAVING COUNT(`item_guid`) > 1);
-- DELETE FROM `mail_items` WHERE `item_guid` IN (@item_guid);

ALTER TABLE `mail_items`
DROP PRIMARY KEY,
ADD PRIMARY KEY(`item_guid`),
ADD KEY `idx_mail_id` (`mail_id`);
ALTER TABLE `character_pet`
ADD KEY `idx_slot` (`slot`);
ALTER TABLE `character_pet`
DROP COLUMN `resettalents_cost`,
DROP COLUMN `resettalents_time`;
ALTER TABLE `characters` ADD COLUMN `grantableLevels` tinyint(3) unsigned NOT NULL DEFAULT '0' AFTER `actionBars`;
ALTER TABLE `character_queststatus` ADD `playercount` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0';
-- Replaces MATRON with PATRON title on MALE char.
UPDATE characters SET knownTitles = CONCAT(
SUBSTRING_INDEX(SUBSTRING_INDEX(knownTitles, ' ', 1), ' ', -1), ' ',
SUBSTRING_INDEX(SUBSTRING_INDEX(knownTitles, ' ', 2), ' ', -1), ' ',
SUBSTRING_INDEX(SUBSTRING_INDEX(knownTitles, ' ', 3), ' ', -1), ' ',
(SUBSTRING_INDEX(SUBSTRING_INDEX(knownTitles, ' ', 4), ' ', -1) | 512) &~256, ' ',
SUBSTRING_INDEX(SUBSTRING_INDEX(knownTitles, ' ', 5), ' ', -1), ' ',
SUBSTRING_INDEX(SUBSTRING_INDEX(knownTitles, ' ', 6), ' ', -1))
WHERE SUBSTRING_INDEX(SUBSTRING_INDEX(knownTitles, ' ', 4), ' ', -1) & 256 AND gender = 0;
DELETE FROM `character_aura` WHERE `spell` = 73001;
DROP TABLE IF EXISTS `character_queststatus_seasonal`;
CREATE TABLE `character_queststatus_seasonal` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier',
  `quest` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quest Identifier',
  `event` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Event Identifier',
  PRIMARY KEY (`guid`,`quest`),
  KEY `idx_guid` (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Player System';
DROP TABLE IF EXISTS `bugreport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bugreport` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identifier',
  `type` longtext NOT NULL,
  `content` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Debug System';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bugreport`
--

LOCK TABLES `bugreport` WRITE;
/*!40000 ALTER TABLE `bugreport` DISABLE KEYS */;
/*!40000 ALTER TABLE `bugreport` ENABLE KEYS */;
UNLOCK TABLES;
