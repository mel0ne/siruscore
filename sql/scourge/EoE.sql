DELETE FROM `creature_loot_template` WHERE `entry` IN (15989, 29991) AND `item` IN (44569, 44577);
INSERT INTO `creature_loot_template` (entry, item, ChanceOrQuestChance, lootmode, groupid, mincountOrRef, maxcount) VALUES
(15989, 44569, 100, 1, 0, 1, 1),
(29991, 44577, 100, 1, 0, 1, 1);
UPDATE `creature_template` SET `speed_walk` = 1.44, `speed_run` = 0.514 WHERE `entry` IN (30084, 32187);
UPDATE `creature_template` SET `difficulty_entry_1` = 31734, `mechanic_immune_mask` = 650853247 WHERE `entry` = 28859;
UPDATE `creature_template` SET `speed_walk` = 4, `speed_run` = 2.85714, `mindmg` = 496, `maxdmg` = 674, `attackpower` = 783, `dmg_multiplier` = 70, `unit_class` = 2, `minrangedmg` = 365, `maxrangedmg` = 529, `rangedattackpower` = 98, `flags_extra` = 1, `mechanic_immune_mask` = 650853247, `InhabitType` = 5 WHERE `entry` = 31734;
UPDATE `creature` SET `spawnMask` = 3, `spawntimesecs` = 604800 WHERE `id` = 28859;
UPDATE `creature_template` SET `AIName` = '', `ScriptName` = 'npc_scion_of_eternity' WHERE `entry` = 30249;
DELETE FROM `smart_scripts` WHERE `entryorguid` = 30249 AND `source_type` = 0;
DELETE FROM `gameobject` WHERE `id` = 193960;
INSERT INTO `gameobject` (id, map, spawnMask, phaseMask, position_x, position_y, position_z, orientation, rotation0, rotation1, rotation2, rotation3, spawntimesecs, animprogress, state) VALUES
(193960, 616, 2, 1, 754.255, 1301.72, 266.17, 5.82666, 0, 0, 0.226287, -0.974061, 120, 0, 1);
DELETE FROM `spelldifficulty_dbc` WHERE `id` IN (8001, 8002, 8003);
INSERT INTO `spelldifficulty_dbc` (id, spellid0, spellid1, spellid2, spellid3) VALUES
(8001, 56272, 60072, 0, 0),
(8002, 57459, 61694, 0, 0),
(8003, 57058, 60073, 0, 0);
DELETE FROM `spell_script_names` WHERE `spell_id` IN (57459, 61694);
INSERT INTO `spell_script_names` (spell_id, ScriptName) VALUES
(57459, 'spell_malygos_arcane_storm'),
(61694, 'spell_malygos_arcane_storm');
UPDATE `creature_template` SET `unit_flags` = 33554434, `InhabitType` = 5 WHERE `entry` = 30334;
DELETE FROM `creature_addon` WHERE `guid` IN (SELECT `guid` FROM `creature` WHERE `id` = 30334);
DELETE FROM `creature` WHERE `id` = 30334;
UPDATE `creature_template` SET `spell1` = 56091, `spell2` = 56092, `spell3` = 57090, `spell4` = 57143, `spell5` = 57108, `spell6` = 57092, `spell7` = 60534, `VehicleId` = 220, `InhabitType` = 5 WHERE `entry` = 31752;
UPDATE `creature_template` SET `faction_A` = 35, `faction_H` = 35, `npcflag` = 16777216, `VehicleId` = 224, `InhabitType` = 5 WHERE `entry` IN (30234, 30248, 31748, 31749);
DELETE FROM `npc_spellclick_spells` WHERE `npc_entry` IN (31749, 30248, 30234, 31748);
INSERT INTO `npc_spellclick_spells` (npc_entry, spell_id, cast_flags, user_type) VALUES
(30234, 61421, 0, 0),
(31748, 61421, 0, 0),
(31749, 61421, 0, 0),
(30248, 61421, 0, 0);
UPDATE `creature_template` SET `Health_mod` = 13 WHERE `entry` = 30249;
UPDATE `creature_template` SET `Health_mod` = 19.5 WHERE `entry` = 31751;
DELETE FROM `creature_text` WHERE `entry` = 28859 AND `groupid` = 15;
INSERT INTO `creature_text` (entry, groupid, id, text, type, language, probability, emote, duration, sound, comment) VALUES
(28859, 15, 0, 'Малигос сосредотачивает свою энергию на на вас!', 5, 0, 100, 0, 0, 0, '');
DELETE FROM `spell_script_names` WHERE `spell_id` IN (57459, 61694);
INSERT INTO `spell_script_names` (spell_id, ScriptName) VALUES
(57459, 'spell_malygos_arcane_storm'),
(61694, 'spell_malygos_arcane_storm');
DELETE FROM `disables` WHERE `sourceType` = 4 AND `entry` IN (7174, 7175);
INSERT INTO `disables` (sourceType, entry, flags, params_0, params_1, comment) VALUES
(4, 7174, 0, 0, 0, ''),
(4, 7175, 0, 0, 0, '');
DELETE FROM `disables` WHERE `sourceType` = 6 AND `entry` = 616;
INSERT INTO `disables` (sourceType, entry, flags, params_0, params_1, comment) VALUES
(6, 616, 4, '', '', '');
UPDATE `creature_template` SET `InhabitType` = 5 WHERE `entry` = 28859;
UPDATE `creature_template` SET `spell1` = 0 WHERE `entry` = 28859;
UPDATE `creature_template` SET `spell2` = 0 WHERE `entry` = 28859;
UPDATE `creature_template` SET `spell3` = 0 WHERE `entry` = 28859;
UPDATE `creature_template` SET `spell4` = 0 WHERE `entry` = 28859;
UPDATE `creature_template` SET `spell5` = 0 WHERE `entry` = 28859;
UPDATE `creature_template` SET `spell6` = 0 WHERE `entry` = 28859;
UPDATE `creature_template` SET `spell7` = 0 WHERE `entry` = 28859;
UPDATE `creature_template` SET `spell8` = 0 WHERE `entry` = 28859;
UPDATE `creature_template` SET `unit_flags` = 320 WHERE `entry` = 28859;
DELETE FROM `creature` WHERE `id` = 28859 AND `map`=616;
INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES (10202237, 28859, 616, 3, 1, 26752, 0, 808.535, 1213.55, 295.972, 3.22503, 604800, 5, 0, 6972500, 212900, 1, 0, 0, 0);
UPDATE `creature_template` SET `unit_flags` = 33554432, `modelid2` = 0 WHERE `entry` = 30090;
DELETE FROM `creature_template` WHERE `entry` = 30118;
INSERT INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `HoverHeight`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `equipment_id`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES (30118, 0, 0, 0, 0, 0, 169, 14501, 0, 0, 'Portal (Malygos)', '', '', 0, 80, 80, 2, 114, 114, 0, 1, 1.14286, 1, 0, 422, 586, 0, 642, 7.5, 2000, 0, 1, 33555200, 8, 0, 0, 0, 0, 0, 345, 509, 103, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 5, 1, 1.35, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 'npc_portal_eoe', 12340);
DELETE FROM `creature_template` WHERE `entry` = 30090;
INSERT INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `HoverHeight`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `equipment_id`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES (30090, 0, 0, 0, 0, 0, 11686, 0, 0, 0, 'Vortex', '', '', 0, 80, 80, 2, 14, 14, 0, 1.2, 0.428571, 1, 0, 422, 586, 0, 642, 7.5, 2000, 0, 1, 33554432, 8, 0, 0, 0, 0, 0, 345, 509, 103, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 214, 0, 0, '', 0, 4, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 76, 1, 0, 0, 130, '', 12340);
DELETE FROM `creature_template` WHERE `entry` = 30334;
INSERT INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `HoverHeight`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `equipment_id`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES (30334, 0, 0, 0, 0, 0, 169, 11686, 0, 0, 'Surge of Power', '', '', 0, 80, 80, 2, 14, 14, 0, 1, 1.14286, 1, 0, 422, 586, 0, 642, 7.5, 2000, 0, 1, 33554434, 8, 0, 0, 0, 0, 0, 345, 509, 103, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 5, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 130, '', 12340);
DELETE FROM `creature` WHERE `map` = 616;
INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES (116911, 31253, 616, 3, 1, 27401, 0, 754.255, 1301.72, 266.253, 1.23918, 3600, 5, 0, 3052, 0, 1, 0, 0, 0);
INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES (116909, 30090, 616, 3, 1, 0, 0, 754.733, 1301.51, 283.379, 5.58505, 3600, 0, 0, 12600, 0, 0, 0, 0, 0);
INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES (116908, 30090, 616, 3, 1, 0, 0, 754.521, 1301.23, 279.524, 0.680678, 3600, 0, 0, 12600, 0, 0, 0, 0, 0);
INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES (116907, 30090, 616, 3, 1, 0, 0, 754.356, 1301.48, 285.733, 5.96903, 3600, 0, 0, 12600, 0, 0, 0, 0, 0);
INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES (116906, 30090, 616, 3, 1, 0, 0, 754.192, 1301.18, 281.851, 5.75959, 3600, 0, 0, 12600, 0, 0, 0, 0, 0);
INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES (116905, 30090, 616, 3, 1, 0, 0, 754.688, 1301.8, 287.295, 1.25664, 3600, 0, 0, 12600, 0, 0, 0, 0, 0);
INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES (116904, 30118, 616, 3, 1, 14501, 0, 652.417, 1200.52, 295.972, 0.785398, 3600, 0, 0, 17010, 0, 0, 0, 0, 0);
INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES (116903, 30118, 616, 3, 1, 14501, 0, 847.67, 1408.05, 295.972, 3.97935, 3600, 0, 0, 17010, 0, 0, 0, 0, 0);
INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES (116902, 30118, 616, 3, 1, 14501, 0, 647.675, 1403.8, 295.972, 5.53269, 3600, 0, 0, 17010, 0, 0, 0, 0, 0);
INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES (116901, 30118, 616, 3, 1, 14501, 0, 843.182, 1215.42, 295.972, 2.35619, 3600, 0, 0, 17010, 0, 0, 0, 0, 0);
INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES (10202237, 28859, 616, 3, 1, 26752, 0, 808.535, 1213.55, 295.972, 3.22503, 604800, 5, 0, 6972500, 212900, 1, 0, 0, 0);
INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES (116899, 32448, 616, 3, 1, 27401, 0, 754.544, 1301.71, 220.083, 3.9968, 3600, 5, 0, 3052, 0, 1, 0, 0, 0);
INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES (116898, 22517, 616, 3, 1, 16925, 0, 754.395, 1301.27, 266.254, 1.0821, 3600, 0, 0, 4120, 0, 0, 0, 0, 0);
INSERT INTO `creature_addon` (`guid`, `path_id`, `mount`, `bytes1`, `bytes2`, `emote`, `auras`) VALUES (116911, 0, 0, 0, 1, 0, NULL);
INSERT INTO `creature_addon` (`guid`, `path_id`, `mount`, `bytes1`, `bytes2`, `emote`, `auras`) VALUES (116909, 0, 0, 50331648, 1, 0, '55883');
INSERT INTO `creature_addon` (`guid`, `path_id`, `mount`, `bytes1`, `bytes2`, `emote`, `auras`) VALUES (116908, 0, 0, 50331648, 1, 0, '55883');
INSERT INTO `creature_addon` (`guid`, `path_id`, `mount`, `bytes1`, `bytes2`, `emote`, `auras`) VALUES (116907, 0, 0, 50331648, 1, 0, '55883');
INSERT INTO `creature_addon` (`guid`, `path_id`, `mount`, `bytes1`, `bytes2`, `emote`, `auras`) VALUES (116906, 0, 0, 50331648, 1, 0, '55883');
INSERT INTO `creature_addon` (`guid`, `path_id`, `mount`, `bytes1`, `bytes2`, `emote`, `auras`) VALUES (116905, 0, 0, 50331648, 1, 0, '55883');
INSERT INTO `creature_addon` (`guid`, `path_id`, `mount`, `bytes1`, `bytes2`, `emote`, `auras`) VALUES (116904, 0, 0, 50331648, 1, 0, '55949');
INSERT INTO `creature_addon` (`guid`, `path_id`, `mount`, `bytes1`, `bytes2`, `emote`, `auras`) VALUES (116903, 0, 0, 50331648, 1, 0, '55949');
INSERT INTO `creature_addon` (`guid`, `path_id`, `mount`, `bytes1`, `bytes2`, `emote`, `auras`) VALUES (116902, 0, 0, 50331648, 1, 0, '55949');
INSERT INTO `creature_addon` (`guid`, `path_id`, `mount`, `bytes1`, `bytes2`, `emote`, `auras`) VALUES (116901, 0, 0, 50331648, 1, 0, '55949');
INSERT INTO `creature_addon` (`guid`, `path_id`, `mount`, `bytes1`, `bytes2`, `emote`, `auras`) VALUES (10202237, 0, 0, 50331648, 1, 0, NULL);
INSERT INTO `creature_addon` (`guid`, `path_id`, `mount`, `bytes1`, `bytes2`, `emote`, `auras`) VALUES (116899, 0, 0, 50331648, 1, 0, NULL);
INSERT INTO `creature_addon` (`guid`, `path_id`, `mount`, `bytes1`, `bytes2`, `emote`, `auras`) VALUES (116898, 0, 0, 0, 1, 0, NULL);