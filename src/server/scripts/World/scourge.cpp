/* Copyright (C) 2008-2010 Scourge <https://scourge.su/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <stdlib.h>

#include "ScriptPCH.h"
#include "SpellScript.h"

/*######
# npc_bg_mine
######*/
#define SAY_1                         -1901107
#define SAY_2                         -1901108
#define SAY_3                         -1901109

class npc_bg_c4 : public CreatureScript
{
    public:
    npc_bg_c4() : CreatureScript("npc_bg_c4") { }

    CreatureAI *GetAI(Creature *creature) const
    {
        return new npc_bg_c4AI(creature);
    }

    struct npc_bg_c4AI : public ScriptedAI
    {
        npc_bg_c4AI(Creature* pCreature) : ScriptedAI(pCreature) { }

        uint32 uiSay1;
        uint32 uiSay2;
        uint32 uiSay3;
        uint32 uiBang;
        uint32 uiDespawn;
        bool bInProcess;

        void Reset()
        {
            uiSay1 = 1000;
            uiSay2 = 2000;
            uiSay3 = 3000;
            uiBang = 4000;
            uiDespawn = 6000;
            bInProcess = true;
        }

        void UpdateAI(const uint32 diff)
        {
            if(uiSay1 <= diff)
            {
                uiSay1 = 1000000;
                DoScriptText(SAY_1, me);
            } else uiSay1 -= diff;

            if(uiSay2 <= diff)
            {
                uiSay2 = 1000000;
                DoScriptText(SAY_2, me);
            } else uiSay2 -= diff;

            if(uiSay3 <= diff)
            {
                uiSay3 = 1000000;
                DoScriptText(SAY_3, me);
            } else uiSay3 -= diff;

            if(bInProcess && uiBang && uiBang <= diff)
            {
                DoCast(me, 90201, true);
                DoCast(me, 90202, true);
                DoCast(me, 90210, true);
                DoCast(me, 90204, true);
                uiBang = 0;
                bInProcess = false;
            } else uiBang -= diff;

            if(uiDespawn && uiDespawn <= diff)
            {
                uiDespawn = 0;
                me->DespawnOrUnsummon();
            } else uiDespawn -= diff;
        }
    };
};




class npc_bg_mine : public CreatureScript
{
    public:
    npc_bg_mine() : CreatureScript("npc_bg_mine") { }

    CreatureAI *GetAI(Creature *creature) const
    {
        return new npc_bg_mineAI(creature);
    }

    struct npc_bg_mineAI : public ScriptedAI
    {
        npc_bg_mineAI(Creature* pCreature) : ScriptedAI(pCreature) { }

        uint32 uiHide;
        uint32 uiBang;
		uint32 uiDespawn;
		bool	bBang;
        bool bDesp;

        void Reset()
        {
            uiHide = 5000;
            uiBang = 10500;
      			bBang = false;
      			bDesp = false;
            uiDespawn = 1000;

        }

        void UpdateAI(const uint32 diff)
        {
            if(uiHide <= diff && !bBang)
            {
                DoCast(me, 1784, true);
                bBang = true;
                uiBang = 500;
            } else uiHide -= diff;

            if(uiBang <= diff)
            {
            
                Map::PlayerList const& pList = me->GetMap()->GetPlayers();
    		        if (!pList.isEmpty())
                {
                    for (Map::PlayerList::const_iterator itr = pList.begin(); itr != pList.end(); ++itr)
                    { 
                            if(me->GetDistance(itr->getSource()->GetPositionX(), itr->getSource()->GetPositionY(), itr->getSource()->GetPositionZ()) <= 5.0f )
                            {
                                DoCast(me, 90205, true);
                                DoCast(itr->getSource(), 90206, true);
                                bDesp = true;
                                uiDespawn = 1500;
                                break;
                            }
					          }
					
                }

                uiBang = 500;
            } else uiBang -= diff;
            
            if(bDesp && uiDespawn <= diff)
            {
                me->DespawnOrUnsummon();
            } 
            else if (bDesp) 
                  uiDespawn -= diff;
        }
        
        
    };
};




class npc_bg_turret : public CreatureScript
{
    public:
    npc_bg_turret() : CreatureScript("npc_bg_turret") { }

    CreatureAI *GetAI(Creature *creature) const
    {
        return new npc_bg_turretAI(creature);
    }

    struct npc_bg_turretAI : public ScriptedAI
    {
        npc_bg_turretAI(Creature* pCreature) : ScriptedAI(pCreature) { }

        uint32 uiFire;

        void Reset()
        {
            uiFire = 500;
        }
        
        void EnterCombat(Unit* /*who*/)
        {
            uiFire = 500;
        }

        void UpdateAI(const uint32 diff)
        {
            if(uiFire <= diff )
            {   
                Unit* target = SelectTarget(SELECT_TARGET_NEAREST, 0, 45, true);
                if(target)
                {
                  me->AddThreat(target, 10000.0f);
                  DoCast(target, 90207, true);
                }
                uiFire = 500;
            } else uiFire -= diff;

        }
    };
};




class npc_nuclear_bomb : public CreatureScript
{
    public:
    npc_nuclear_bomb() : CreatureScript("npc_nuclear_bomb") { }

    CreatureAI *GetAI(Creature *creature) const
    {
        return new npc_nuclear_bombAI(creature);
    }

    struct npc_nuclear_bombAI : public ScriptedAI
    {
        npc_nuclear_bombAI(Creature* pCreature) : ScriptedAI(pCreature) { }

        uint32 uiAttention;
        uint32 uiArmageddon;
		bool   bAttention;
		bool   bArmageddon;
        

        void Reset()
        {
            uiAttention = 1000;
            uiArmageddon = 5000; 
			bAttention = true;
			bArmageddon = false;
        }


        void UpdateAI(const uint32 diff)
        {
            if(bAttention && uiAttention <= diff )
            {   

                bAttention = false;
				bArmageddon = true;
            } else uiAttention -= diff;
            
            if(bArmageddon && uiArmageddon <= diff )
            {   
                DoCast(me, 90120, true);
                bArmageddon = false;
            } else uiArmageddon -= diff;            

        }
    };
};





class npc_bg_fort_guard : public CreatureScript
{
    public:
    npc_bg_fort_guard() : CreatureScript("npc_bg_fort_guard") { }

    CreatureAI *GetAI(Creature *creature) const
    {
        return new npc_bg_fort_guardAI(creature);
    }

    struct npc_bg_fort_guardAI : public ScriptedAI
    {
        npc_bg_fort_guardAI(Creature* pCreature) : ScriptedAI(pCreature) { }

        uint32 uiGoAway;
        uint32 uiShield;

        void Reset()
        {
            uiGoAway = 500;
            uiShield = 1000;
        }


        void UpdateAI(const uint32 diff)
        {
            if(uiGoAway <= diff )
            {   
                Map::PlayerList const& pList = me->GetMap()->GetPlayers();
        				if (!pList.isEmpty())
                    for (Map::PlayerList::const_iterator itr = pList.begin(); itr != pList.end(); ++itr)
                    { 
                        if (itr->getSource()->getFaction() != me->getFaction())
                            if(me->GetDistance(itr->getSource()->GetPositionX(), itr->getSource()->GetPositionY(), itr->getSource()->GetPositionZ()) <= 5.0f )
                            {
                                DoCast(me, 90205, true);
                            }
                  }
                  
                uiGoAway = 500;
            } else uiGoAway -= diff;
            
            if(uiShield <= diff )
            {   
               DoCast(me, 90200, true); 
               uiShield = 1000;
            }

        }
    };
};



enum eAirStrike
{
    NPC_BOMBER          = 50003,
    SPELL_RED_FLARE     = 30342,
    SPELL_BOBM_DROP     = 90208,
    SPELL_BOBM_EXLP     = 90209,
    SPELL_EXPL_EFFECT   = 90204,
      


};
       
class npc_bg_air_strike : public CreatureScript
{
    public:
    npc_bg_air_strike() : CreatureScript("npc_bg_air_strike") { }

    CreatureAI *GetAI(Creature *creature) const
    {
        return new npc_bg_air_strikeAI(creature);
    }

    struct npc_bg_air_strikeAI : public ScriptedAI
    {
        npc_bg_air_strikeAI(Creature* pCreature) : ScriptedAI(pCreature), Summons(me) { }

        uint32 uiSummon;
        uint32 uiStrike;
        uint32 uiUnsummon;
        uint32 uiEffect;
        
        bool bEffect;        
        
        Creature* cBomber;
        
        bool bSummoned;
        bool bStriked;
        
        SummonList Summons;

        
        void Reset()
        {   
        
            DoCast(me, SPELL_RED_FLARE, true);
            uiSummon = 5000;
            uiStrike = 5000;
            uiUnsummon = 1500;
            uiEffect = 2500;
            bSummoned = false;
            bStriked = false;
            bEffect = false;
            Summons.DespawnAll();
        }
        
        void JustSummoned(Creature* summoned)
        {
                       /*
            summoned->SetCanFly(true);
            summoned->GetMotionMaster()->MovePoint(0, me->GetPositionX()+30, me->GetPositionY()+30, me->GetPositionZ()+35);
            summoned->SetSpeed(MOVE_FLIGHT, 1.0f);     */


            Summons.Summon(summoned);
        }     
        
        
        void SpellHit(Unit* caster, const SpellInfo* proto)
        {
            if (!proto)
                return;

            if (!caster)
                return;
                
                
            if (proto->Id == SPELL_BOBM_DROP)
            {
                bEffect = true;
                uiEffect = 1000;
            }
        }   


        void UpdateAI(const uint32 diff)
        {
            if(uiSummon <= diff && !bSummoned)
            {   
                cBomber = me->SummonCreature(NPC_BOMBER, me->GetPositionX()-30, me->GetPositionY()-30, me->GetPositionZ()+35, 0.0f, TEMPSUMMON_TIMED_DESPAWN, 20000);
                if(cBomber)
                {
                    cBomber->SetCanFly(true);
                    cBomber->GetMotionMaster()->MovePoint(0, me->GetPositionX()+30, me->GetPositionY()+30, me->GetPositionZ()+35);
                    cBomber->SetSpeed(MOVE_FLIGHT, 1.0f);
                    bSummoned = true;
                }
                uiSummon = 5000;
            } else uiSummon -= diff;
            
            if(uiStrike <= diff && !bStriked)
            { 
                if(cBomber)
                {
                    if(cBomber->GetDistance(me->GetPositionX(), me->GetPositionY(), me->GetPositionZ()) <= 35.0f)
                    {
                        cBomber->AI()->DoCast(me, SPELL_BOBM_DROP, true);
                        uiUnsummon = 5000;
                        bStriked = true;                      
                    }

                }  

               uiStrike = 400;
            }
            else uiStrike -= diff;

            if(uiUnsummon <= diff && bStriked)
            {
                if(cBomber)
                {
                   Summons.DespawnAll();
                   me->DespawnOrUnsummon(); 
                }

            }
            else if (bStriked)
                uiUnsummon -= diff;
            
            
            if(uiEffect <= diff && bEffect)
            {
                DoCast(me, SPELL_EXPL_EFFECT, true);
                DoCast(me, SPELL_BOBM_EXLP, true);
                bEffect = false;
            }
            else uiEffect -= diff;

        }
    };
};


float BgPos[6][3] = 
{
	{989.1712040f, 1039.718018f, -42.827442f},
	{1240.775879f, 1478.783325f, 309.616150f},
	{-270.683899f, -284.885376f, 6.8813380f},
	{2174.142822f, 1569.614014f, 1159.960693f},
	{1238.036865f, -65.4986110f, 70.084290f},
	{776.005432f, -646.3651730f, 9.8099750f}
};

#define NPC_NC_BOMB             50008

class item_nc_bomb : public ItemScript
{
    public:
    item_nc_bomb() : ItemScript("item_nc_bomb") { }

    bool OnUse(Player* pPlayer, Item* pItem, SpellCastTargets const& targets)
    {
    
        sLog->outError("ZONE =>>>> %d", pPlayer->GetZoneId());
        float x, y, z;
        
        x = pPlayer->GetPositionX();
        y = pPlayer->GetPositionY();
        z = pPlayer->GetPositionZ();


        
        switch(pPlayer->GetZoneId())
        {
            // ������ �����
            case 3358:
                x = BgPos[0][0];
                y = BgPos[0][1];
                z = BgPos[0][2];

                break;
            // ���
            case 3277:
                x = BgPos[1][0];
                y = BgPos[1][1];
                z = BgPos[1][2];
                                
                break;
            // ����
            case 2597:
                x = BgPos[2][0];
                y = BgPos[2][1];
                z = BgPos[2][2]; 
                                
                break;
            // ���
            case 3820:
                x = BgPos[3][0];
                y = BgPos[3][1];
                z = BgPos[3][2];
                                
                break;
            // �����
            case 4384:
                x = BgPos[4][0];
                y = BgPos[4][1];
                z = BgPos[4][2];
            
                break;
            // ������
            case 4710:
                x = BgPos[5][0];
                y = BgPos[5][1];
                z = BgPos[5][2];
            
                break;
                
            default:
            
                break;
            
        
        }

        sLog->outString("ZONE =>>>> %d", pPlayer->GetZoneId());
        
        pPlayer->SummonCreature(NPC_NC_BOMB, x, y, z, 0, TEMPSUMMON_TIMED_DESPAWN, 20000);
              
        
        return true;
    }
};




/*#############
# item_bg_fort
#############*/
#define NPC_FORT_GUARD             999140

class item_bg_fort : public ItemScript
{
    public:
    item_bg_fort() : ItemScript("item_bg_fort") { }

    bool OnUse(Player* pPlayer, Item* pItem, SpellCastTargets const& targets)
    {
        if (pPlayer->InBattleground())
        {
            //pPlayer->DoCast(pPlayer, 30263, true);
            Unit* pFortGuard = NULL;
            pFortGuard = pPlayer->SummonCreature(NPC_FORT_GUARD, pPlayer->GetPositionX(), pPlayer->GetPositionY(), pPlayer->GetPositionZ(), 0, TEMPSUMMON_TIMED_DESPAWN, 120000);

        }
        return false;
    }
};


/*#############
# item_bg_air_strike
#############*/
#define NPC_FORT_GUARD             999140

class item_bg_predator : public ItemScript
{
    public:
    item_bg_predator() : ItemScript("item_bg_predator") { }

    bool OnUse(Player* pPlayer, Item* pItem, SpellCastTargets const& targets)
    {
        if (pPlayer->InBattleground())
        {
            Unit* pPredator = NULL;
            pPredator = pPlayer->SummonCreature(NPC_FORT_GUARD, pPlayer->GetPositionX(), pPlayer->GetPositionY(), pPlayer->GetPositionZ(), 0, TEMPSUMMON_TIMED_DESPAWN, 120000);
            if(pPredator)
            {
        				pPredator->setFaction(pPlayer->getFaction());
        				pPlayer->EnterVehicle(pPredator);
        				pItem->DestroyForPlayer(pPlayer);
            }
        }
        return false;
    }
};


/*#############
# item_bg_orbital_strike
#############*/
#define NPC_ORBITAL_STRIKE             150015

class item_bg_orbital_strike : public ItemScript
{
    public:
    item_bg_orbital_strike() : ItemScript("item_bg_orbital_strike") { }

    bool OnUse(Player* pPlayer, Item* pItem, SpellCastTargets const& targets)
    {
        if (pPlayer->InBattleground())
        {
            Unit* pOrbitalStrike = NULL;
            pOrbitalStrike = pPlayer->SummonCreature(NPC_ORBITAL_STRIKE, pPlayer->GetPositionX(), pPlayer->GetPositionY(), pPlayer->GetPositionZ(), 0, TEMPSUMMON_TIMED_DESPAWN, 300000);

            if(pOrbitalStrike)
            {
				pOrbitalStrike->setFaction(pPlayer->getFaction());
				pPlayer->EnterVehicle(pOrbitalStrike);
				pItem->DestroyForPlayer(pPlayer);
            }
        }
        return false;
    }
};

/*#############
# item_bg_btr
#############*/
#define NPC_BTR                        150011

class item_bg_btr : public ItemScript
{
    public:
    item_bg_btr() : ItemScript("item_bg_btr") { }

    bool OnUse(Player* pPlayer, Item* pItem, SpellCastTargets const& targets)
    {
        if (pPlayer->InBattleground())
            if (Unit* pVehicle = pPlayer->SummonCreature(NPC_BTR, pPlayer->GetPositionX() + 3.0f, pPlayer->GetPositionY(), pPlayer->GetPositionZ() + 7.0f, pPlayer->GetOrientation(),
            TEMPSUMMON_TIMED_DESPAWN, 300000))
			{
				pVehicle->setFaction(pPlayer->getFaction());
                pPlayer->EnterVehicle(pVehicle);
			}
        pItem->DestroyForPlayer(pPlayer);
        return false;
    }
};

/*#############
# item_bg_flying_machine
#############*/
#define NPC_COBRA_MACHINE              150003

class item_bg_flying_machine : public ItemScript
{
    public:
    item_bg_flying_machine() : ItemScript("item_bg_flying_machine") { }

    bool OnUse(Player* pPlayer, Item* pItem, SpellCastTargets const& targets)
    {
        if (pPlayer->InBattleground())
            if (Unit* pVehicle = pPlayer->SummonCreature(NPC_COBRA_MACHINE, pPlayer->GetPositionX() + 3.0f, pPlayer->GetPositionY(), pPlayer->GetPositionZ() + 7.0f, pPlayer->GetOrientation(),
            TEMPSUMMON_TIMED_DESPAWN, 300000))
                if(pVehicle->IsVehicle())
                    pPlayer->EnterVehicle(pVehicle);
        pItem->DestroyForPlayer(pPlayer);
        return false;
    }
};

/*#############
# item_bg_mine
#############*/
#define NPC_MINE                       600901

class item_bg_mine : public ItemScript
{
    public:
    item_bg_mine() : ItemScript("item_bg_mine") { }

    bool OnUse(Player* pPlayer, Item* pItem, SpellCastTargets const& targets)
    {
        if (pPlayer->InBattleground())
            if (Unit* pMine = pPlayer->SummonCreature(NPC_MINE, pPlayer->GetPositionX(), pPlayer->GetPositionY(), pPlayer->GetPositionZ(), pPlayer->GetOrientation(), TEMPSUMMON_DEAD_DESPAWN,
            5000))
                pMine->setFaction(pPlayer->getFaction());

        return false;
    }
};


/*######
## npc_emblem_changer
######*/

#define REMOVE_EMBLEM_COUNT           3
#define REQUIRE_MONEY                 300000 // 30 gold
#define RECEIVE_EMBLEM_COUNT          1
#define ITEM_EMBLEM_OF_HEROISM        40752
#define ITEM_EMBLEM_OF_VALOR          40753
#define ITEM_EMBLEM_OF_CONQUEST       45624
#define ITEM_EMBLEM_OF_TRIUMPH        47241
#define ITEM_EMBLEM_OF_FROST          49426
#define ITEM_COMMENDATION_OF_SERVICE  54637
#define SPELL_FEAR                    29168
#define ACHIEVEMENT_DUALSPEC          2716
#define ACHIEVEMENT_RIDING_75         891
#define ACHIEVEMENT_RIDING_150        889
#define ACHIEVEMENT_RIDING_225        890
#define ACHIEVEMENT_RIDING_300        892
#define GOSSIP_HEROISM_TO_VALOR       "I want to change 3 Emblem of Heroism to 1 Emblem of Valor."
#define GOSSIP_VALOR_TO_CONQUEST      "I want to change 3 Emblem of Valor to 1 Emblem of Conquest."
#define GOSSIP_CONQUEST_TO_TRIUMPH    "I want to change 3 Emblem of Conquest to 1 Emblem of Triumph."
#define GOSSIP_TRIUMPH_TO_FROST       "I want to change 3 Emblem of Triumph and 30 gold coins to 1 Emblem of Frost."
#define GOSSIP_DONT_HAVE_EMBLEM       "I don't have emblem, ban me please! (This accident will push to Administrators and you will be ban later...sorry, guy. Its too late to close this windows.)"
#define GOSSIP_BG_MEDALS_TO_HONOR     "I want to change all medals from bg to honor."

uint32 uiItemRemove = 0;
uint32 uiItemReceive = 0;
uint32 uiMarkOfHonor[6] = {20560,20559,29024,47395,20558,42425};

class npc_emblem_changer : public CreatureScript
{
    public:
    npc_emblem_changer() : CreatureScript("npc_emblem_changer") { }

    bool OnGossipHello(Player *pPlayer, Creature *pCreature)
    {
        pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_VENDOR, GOSSIP_TEXT_BROWSE_GOODS, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_TRADE);
        pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_HEROISM_TO_VALOR, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+1);
        pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_VALOR_TO_CONQUEST, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+2);
        pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_CONQUEST_TO_TRIUMPH, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+3);
        pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_TRIUMPH_TO_FROST, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+4);
        pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_BG_MEDALS_TO_HONOR, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+5);
        pPlayer->SEND_GOSSIP_MENU(pPlayer->GetGossipTextId(pCreature), pCreature->GetGUID());
        return true;
    }

    bool OnGossipSelect(Player* pPlayer, Creature* pCreature, uint32 /*uiSender*/, uint32 uiAction)
    {
        switch(uiAction)
        {
            case GOSSIP_ACTION_INFO_DEF+1: uiItemRemove = ITEM_EMBLEM_OF_HEROISM; uiItemReceive = ITEM_EMBLEM_OF_VALOR; break;
            case GOSSIP_ACTION_INFO_DEF+2: uiItemRemove = ITEM_EMBLEM_OF_VALOR; uiItemReceive = ITEM_EMBLEM_OF_CONQUEST; break;
            case GOSSIP_ACTION_INFO_DEF+3: uiItemRemove = ITEM_EMBLEM_OF_CONQUEST; uiItemReceive = ITEM_EMBLEM_OF_TRIUMPH; break;
            case GOSSIP_ACTION_INFO_DEF+4: uiItemRemove = ITEM_EMBLEM_OF_TRIUMPH; uiItemReceive = ITEM_EMBLEM_OF_FROST; break;
            case GOSSIP_ACTION_TRADE: pPlayer->GetSession()->SendListInventory(pCreature->GetGUID()); break;
            case GOSSIP_ACTION_INFO_DEF+5:
            {
                for(int i = 0; i < 6; i++)
                {
                    while(pPlayer->HasItemCount(uiMarkOfHonor[i], 1, true))
                    {
                        pPlayer->DestroyItemCount(uiMarkOfHonor[i], 1, true, false);
                        pPlayer->StoreNewItemInBestSlots(ITEM_COMMENDATION_OF_SERVICE, 1);
                    }
                }
                pPlayer->CLOSE_GOSSIP_MENU();
                break;
            }
        }

        if(uiAction == GOSSIP_ACTION_INFO_DEF+1 || uiAction == GOSSIP_ACTION_INFO_DEF+2
            || uiAction == GOSSIP_ACTION_INFO_DEF+3)
        {
            if(uiItemRemove && uiItemReceive
                && pPlayer->HasItemCount(uiItemRemove, REMOVE_EMBLEM_COUNT, true))
            {
                pPlayer->DestroyItemCount(uiItemRemove, REMOVE_EMBLEM_COUNT, true, false);
                pPlayer->StoreNewItemInBestSlots(uiItemReceive, RECEIVE_EMBLEM_COUNT);
                pPlayer->CLOSE_GOSSIP_MENU();
            }
        }

        if (uiAction == GOSSIP_ACTION_INFO_DEF+4)
        {
            if(uiItemRemove && uiItemReceive
                && pPlayer->HasItemCount(uiItemRemove, REMOVE_EMBLEM_COUNT, true) && pPlayer->HasEnoughMoney(REQUIRE_MONEY))
            {
                pPlayer->ModifyMoney(-REQUIRE_MONEY);
                pPlayer->DestroyItemCount(uiItemRemove, REMOVE_EMBLEM_COUNT, true, false);
                pPlayer->StoreNewItemInBestSlots(uiItemReceive, RECEIVE_EMBLEM_COUNT);
                pPlayer->CLOSE_GOSSIP_MENU();
            }

        }

        return true;
    }
};

/*######
## npc_sportcar_trader
######*/

#define GOSSIP_GET_SPORTCAR        "I want to get a sportcar! 15 gold."
#define NPC_SPORTCAR               RAND(8000100, 8000101, 8000102, 8000103, 8000105)
#define MONEY_COST                 15*100*100

class npc_sportcar_trader : public CreatureScript
{
    public:
    npc_sportcar_trader() : CreatureScript("npc_sportcar_trader") { }

    bool OnGossipHello(Player *pPlayer, Creature *pCreature)
    {
        pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_GET_SPORTCAR, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+1);
        pPlayer->SEND_GOSSIP_MENU(pPlayer->GetGossipTextId(pCreature), pCreature->GetGUID());
        return true;
    }

    bool OnGossipSelect(Player* pPlayer, Creature* /*pCreature*/, uint32 /*uiSender*/, uint32 uiAction)
    {
        if(uiAction == GOSSIP_ACTION_INFO_DEF+1)
        {
            if(!pPlayer->HasEnoughMoney(MONEY_COST))
                return true;

            pPlayer->ModifyMoney(-MONEY_COST);
            pPlayer->TeleportTo(1, -6302.967285f, -3907.620850f, -56.8389f, 0);
            Unit* pSportCar = pPlayer->SummonCreature(NPC_SPORTCAR, -6302.967285f, -3907.620850f, -56.8389f, 0, TEMPSUMMON_DEAD_DESPAWN, 5000);
            if(pSportCar && pSportCar->IsVehicle())
            {
                pPlayer->EnterVehicle(pSportCar);
                pSportCar->AddUnitTypeMask(UNIT_MASK_MINION);
            }
        }
        pPlayer->CLOSE_GOSSIP_MENU();
        return true;
    }
};

const uint32 maxAvailableZones = 7;
const uint32 availableZones[maxAvailableZones] = {1519, 2257, 1537, 1, 38, 11, 45};
const uint32 soundid[4] = {8873, 8874, 8875, 8876};
const WorldLocation stormwindloc(0, -8610, 707, 97, 0);
const WorldLocation wetlandloc(0, -2567, -2355, 80, 0);
const WorldLocation dalaranloc(571, 5815, 804, 1072, 4);

#define Q_A 777006
#define Q_B 777012
#define Q_C 777035
#define Q_D 777044
#define Q_F 777045

#define SPECIALAURA 50203

class PsEventStuff : public PlayerScript {
  Timer tpTimer;
  Timer soundTimer;
 public:
  PsEventStuff() : PlayerScript("PsEventStuff") {
    tpTimer.SetTimer(10000);
    soundTimer.SetTimer(60000);
  }

  void OnUpdate(Player* player, const uint32 diff) {
    if (!player->IsInWorld()) return;
    if (!player->HasAura(SPECIALAURA)) return;

    if (tpTimer.CheckTimer(diff))
      DoTP(player);

    if (soundTimer.CheckTimer(diff))
      DoSound(player);
  }

  void OnQuestTaken(Player* player, uint64 qentry) {
    switch (qentry) {
      case Q_A :
        player->TeleportTo(stormwindloc);
        player->CastSpell(player, SPECIALAURA, true);
        break;
      case Q_B :
        player->TeleportTo(wetlandloc);
        break;
    };
  }

  void OnQuestDone(Player* player, uint64 qentry) {
    switch (qentry) {
      case Q_C :
        player->TeleportTo(dalaranloc);
        player->RemoveAurasDueToSpell(SPECIALAURA);
        break;
      case Q_D :
        player->TeleportTo(dalaranloc);
        player->RemoveAurasDueToSpell(SPECIALAURA);
        break;
      case Q_F :
        player->TeleportTo(wetlandloc);
        player->CastSpell(player, SPECIALAURA, true);
        break;
    };
  }

 private:
  void DoTP(Player* player) {
    bool result = false;

    for (int8 n = 0; n != maxAvailableZones && !result; ++n)
      result = player->GetZoneId() == availableZones[n];

    if (result) return;

    if (player->GetQuestStatus(Q_B) == QUEST_STATUS_COMPLETE) {
      player->TeleportTo(wetlandloc);
    } else {
      player->TeleportTo(stormwindloc);
    }
  }

  void DoSound(Player* player) {
    player->PlayDirectSound(soundid[urand(0, 3)], player);
  }
};

class EventTriggerTeleporter : public CreatureScript {
public:
    
    EventTriggerTeleporter() : CreatureScript("EventTriggerTeleporter") { }
    
    struct EventTriggerTeleporterAI : public Scripted_NoMovementAI 
    {
        EventTriggerTeleporterAI(Creature * pCreature) : Scripted_NoMovementAI(pCreature) { }
        
        void MoveInLineOfSight(Unit * pWho) 
        {
            if (!pWho || !pWho->IsInWorld())
                return;
            
            if (!me->IsWithinDist(pWho, 6.0f, false))
                return;
            
            Player *pPlayer = pWho->GetCharmerOrOwnerPlayerOrPlayerItself();
          
            if (!pPlayer || pPlayer->isGameMaster())
                return;
            
            if (pPlayer->GetZoneId() == 2817) 
            { 
                // dalaran
                pPlayer->CastSpell(pPlayer, SPECIALAURA, true);
                if (pPlayer->GetQuestStatus(Q_B) == QUEST_STATUS_COMPLETE) {
                    pPlayer->TeleportTo(wetlandloc); 
                } else {
                    pPlayer->TeleportTo(stormwindloc);
                }
                return;
            }
            
            if (pPlayer->GetZoneId() == 1519) 
            { 
                // stormwind
                pPlayer->CastSpell(pPlayer, SPECIALAURA, true);
                pPlayer->TeleportTo(wetlandloc); // to marshland
                return;
            }
            
            if (pPlayer->GetZoneId() == 2257) 
            { 
                // metro
                pPlayer->RemoveAurasDueToSpell(SPECIALAURA);
                pPlayer->TeleportTo(dalaranloc);
                return;
            }
        }
    };
    
    CreatureAI *GetAI(Creature *creature) const 
    {
        return new EventTriggerTeleporterAI(creature);
    }
};

#define LOW_LEVEL "���� ������� ������� �����!"
#define NOT_ENOUGH_MONEY "� ���� ���� �����!"

class teleport_on_click : public CreatureScript
{
public:
    teleport_on_click() : CreatureScript("teleport_on_click") { }

    struct teleport_on_clickAI : public ScriptedAI
    {
        teleport_on_clickAI(Creature* c) : ScriptedAI(c) { }

        int32 pGold;
        uint32 pFaction;
        uint8 plevel;
        
        void MoveInLineOfSight()
        {
            Player* plr = me->GetCharmerOrOwnerPlayerOrPlayerItself();

            if (plr && me->isTappedBy(plr))
            {
                plevel = plr->getLevel();
                pGold = plr->GetMoney();
                pFaction = plr->getFaction();

                QueryResult res = CharacterDatabase.PQuery("SELECT map,x,y,z,req_money,req_level,req_faction FROM tp_reqs WHERE guid = '%u'",me->GetGUID());

                if (res)
                    return;

                Field* fields = res->Fetch();
                uint32 map = fields[1].GetUInt32();
                float x = fields[2].GetFloat();
                float y = fields[3].GetFloat();
                float z = fields[4].GetFloat();
                int32 reqGold = fields[5].GetInt32();
                uint8 reqLevel = fields[6].GetUInt8();
                uint32 reqFaction = fields[7].GetUInt32();

                if (plevel < reqLevel)
                {
                    me->MonsterWhisper(LOW_LEVEL, plr->GetGUID());
                    return;
                }

                if (pGold < reqGold)
                {
                    me->MonsterWhisper(NOT_ENOUGH_MONEY, plr->GetGUID());
                    return;
                }

                if (pFaction != reqFaction)
                    return;

                if (!plr->isInCombat())
                {
                    plr->TeleportTo(map, x, y, z, 0);
                    plr->ModifyMoney(-reqGold);
                }
            }

        }
    };

    CreatureAI* GetAI(Creature* c) const
    {
        return new teleport_on_clickAI(c);
    }
};

class event_start1 : public CreatureScript
{
public:
    event_start1() : CreatureScript("event_start1") { }

    struct event_startAI : public ScriptedAI
    {
        event_startAI(Creature* c) : ScriptedAI(c) { }

        bool OnQuestAccept(Player* plr, Creature* creature, Quest const* questid, uint32 /*item*/)
        {
                if (questid->GetQuestId() == 777006)
                {
                    plr->TeleportTo(0, -8611.000000f, 706.000000f, 97.392937f, 0);
                    plr->CastSpell(plr, SPECIALAURA, false);
                    return true;
                }

                if (questid->GetQuestId() == 777012)
                {
                    plr->TeleportTo(0, -2592.481934f, -2353.313477f, 83.921616f,0);
                    return true;
                }
            return false;
        }

        bool OnQuestComplete(Player* plr,Creature* creature, Quest const* questid, uint32 /*opt*/)
        {
            if (questid->GetQuestId() == 777035)
            {
                plr->TeleportTo(571, 5797.000000f, 794.000000f, 662.911072f, 0);
                plr->RemoveAurasDueToSpell(SPECIALAURA);
                return true;
            }
            return false;
        }
    };

     CreatureAI* GetAI(Creature* c) const
     {
         return new event_startAI(c);
     }
};

class achiev_kill_333_cows : public AchievementCriteriaScript
{
  public:
    achiev_kill_333_cows() : AchievementCriteriaScript("achiev_kill_333_cows") { }

    bool OnCheck(Player* source, Unit* /*target*/) {
      if (!source->HasSpecialAuras()) return false;
        return true;
    }
};

enum DamagerSpells
{
    SPELL_STOMP			                                   = 60880,
    SPELL_PLAGUE                                           = 54835,
    SPELL_GROWTH										   = 36300,
};

class npc_damager1 : public CreatureScript
{
public:
    npc_damager1() : CreatureScript("npc_damager1") { }

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_damager1AI (creature);
    }

    struct npc_damager1AI : public ScriptedAI
    {
        npc_damager1AI(Creature* creature) : ScriptedAI(creature)
        {
        }

        uint32 Stomp_Timer;
        uint32 Immolate_Timer;
        uint32 Plague_Timer;
        uint32 Growth_Timer;

        void Reset()
        {
            Stomp_Timer				= 5000;
            Plague_Timer		    = 13000;
            Growth_Timer			= 15000;

        }

        void EnterCombat(Unit* /*who*/)
        {
			    Stomp_Timer				= 5000;
		        Plague_Timer		    = 13000;
	            Growth_Timer			= 16000;
				Unit* target = SelectTarget(SELECT_TARGET_NEAREST, 0, 100, true);
				me->AddThreat(target, 10000.0f);
        }

        void EnterEvadeMode()
        {
            me->RemoveAllAuras();
            me->DeleteThreatList();
            me->CombatStop(true);
            me->LoadCreaturesAddon();
            if (me->isAlive())
                me->GetMotionMaster()->MoveTargetedHome();
            me->SetLootRecipient(NULL);
        }

        void JustDied(Unit* /*who*/)
        {
        }

        void UpdateAI(const uint32 diff)
        {

            if (!UpdateVictim())
                return;

            //Stomp_Timer
            if (Stomp_Timer <= diff)
            {
                DoCast(me->getVictim(), SPELL_STOMP);
                Stomp_Timer = 5000;
				Unit* target = SelectTarget(SELECT_TARGET_NEAREST, 0, 100, true);
				me->AddThreat(target, 100000.0f);
            } else Stomp_Timer -= diff;

            //Corruption_Timer
            if (Plague_Timer <= diff)
            {
                DoCast(me->getVictim(), SPELL_PLAGUE);
                Plague_Timer = 15000;
            } else Plague_Timer -= diff;

			//Growth_timer
            if (Growth_Timer <= diff)
            {
                DoCast(me, SPELL_GROWTH);
                Growth_Timer = 15000;
            } else Growth_Timer -= diff;

            DoMeleeAttackIfReady();
        }
    };
};

class npc_damager2 : public CreatureScript
{
public:
    npc_damager2() : CreatureScript("npc_damager2") { }

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_damager2AI (creature);
    }

    struct npc_damager2AI : public ScriptedAI
    {
        npc_damager2AI(Creature* creature) : ScriptedAI(creature)
        {
        }

        uint32 Stomp_Timer;
        uint32 Immolate_Timer;
        uint32 Plague_Timer;
        uint32 Growth_Timer;

        void Reset()
        {
            Stomp_Timer				= 4000;
            Plague_Timer		    = 12000;
            Growth_Timer			= 15000;

        }

        void EnterCombat(Unit* /*who*/)
        {
			    Stomp_Timer				= 4000;
		        Plague_Timer		    = 12000;
	            Growth_Timer			= 15000;
				Unit* target = SelectTarget(SELECT_TARGET_NEAREST, 0, 100, true);
				me->AddThreat(target, 10000.0f);
        }

        void EnterEvadeMode()
        {
            me->RemoveAllAuras();
            me->DeleteThreatList();
            me->CombatStop(true);
            me->LoadCreaturesAddon();
            if (me->isAlive())
                me->GetMotionMaster()->MoveTargetedHome();
            me->SetLootRecipient(NULL);
        }

        void JustDied(Unit* /*who*/)
        {
        }

        void UpdateAI(const uint32 diff)
        {

            if (!UpdateVictim())
                return;

            //Stomp_Timer
            if (Stomp_Timer <= diff)
            {
                DoCast(me->getVictim(), SPELL_STOMP);
                Stomp_Timer = urand(3000,7000);
				Unit* target = SelectTarget(SELECT_TARGET_NEAREST, 0, 100, true);
				me->AddThreat(target, 100000.0f);
            } else Stomp_Timer -= diff;

            //Corruption_Timer
            if (Plague_Timer <= diff)
            {
                DoCast(me->getVictim(), SPELL_PLAGUE);
                Plague_Timer = urand(11000,19000);
            } else Plague_Timer -= diff;

			//Growth_timer
            if (Growth_Timer <= diff)
            {
                DoCast(me, SPELL_GROWTH);
                Growth_Timer = urand(11000,19000);
            } else Growth_Timer -= diff;

            DoMeleeAttackIfReady();
        }
    };
};

enum DoomrelSpells
{
    SPELL_SHADOWBOLTVOLLEY                                 = 15245,
    SPELL_IMMOLATE                                         = 12742,
    SPELL_CURSEOFWEAKNESS                                  = 12493,
    SPELL_DEMONARMOR                                       = 13787,
    SPELL_SUMMON_VOIDWALKERS                               = 15092
};

class npc_healtarget1 : public CreatureScript
{
public:
    npc_healtarget1() : CreatureScript("npc_healtarget1") { }

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_healtarget1AI (creature);
    }

    struct npc_healtarget1AI : public ScriptedAI
    {
        npc_healtarget1AI(Creature* creature) : ScriptedAI(creature)
        {
        }

        uint32 ShadowVolley_Timer;
        uint32 Immolate_Timer;
        uint32 CurseOfWeakness_Timer;
        uint32 DemonArmor_Timer;

        void Reset()
        {
            ShadowVolley_Timer      = 10000;
            Immolate_Timer          = 18000;
            CurseOfWeakness_Timer   = 5000;
            DemonArmor_Timer        = 16000;
        }

        void EnterCombat(Unit* /*who*/)
        {
			    ShadowVolley_Timer      = 10000;
			    Immolate_Timer          = 18000;
		        CurseOfWeakness_Timer   = 5000;
	            DemonArmor_Timer        = 16000;
        }

        void EnterEvadeMode()
        {
            me->RemoveAllAuras();
            me->DeleteThreatList();
            me->CombatStop(true);
            me->LoadCreaturesAddon();
            if (me->isAlive())
                me->GetMotionMaster()->MoveTargetedHome();
            me->SetLootRecipient(NULL);
        }

        void JustDied(Unit* /*who*/)
        {
        }

        void UpdateAI(const uint32 diff)
        {
			if (!me->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_IN_COMBAT))
			{
				me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_IN_COMBAT);
			}
		
            //ShadowVolley_Timer
            if (ShadowVolley_Timer <= diff)
            {
                DoCast(me->getVictim(), SPELL_SHADOWBOLTVOLLEY);
                ShadowVolley_Timer = 12000;
            } else ShadowVolley_Timer -= diff;

            //Immolate_Timer
            if (Immolate_Timer <= diff)
            {
                if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 100, true))
                    DoCast(target, SPELL_IMMOLATE);

                Immolate_Timer = 25000;
            } else Immolate_Timer -= diff;

            //CurseOfWeakness_Timer
            if (CurseOfWeakness_Timer <= diff)
            {
                DoCast(me->getVictim(), SPELL_CURSEOFWEAKNESS);
                CurseOfWeakness_Timer = 45000;
            } else CurseOfWeakness_Timer -= diff;

            //DemonArmor_Timer
            if (DemonArmor_Timer <= diff)
            {
                DoCast(me, SPELL_DEMONARMOR);
                DemonArmor_Timer = 300000;
            } else DemonArmor_Timer -= diff;

            DoMeleeAttackIfReady();
        }
    };
};

//Nuclear Bomb
#define BOMB 100912 //TAKE TRUE ID
class nuclear_bomb : public ItemScript
{
public:
    nuclear_bomb() : ItemScript("nuclear_bomb") { }

    bool OnUse(Player* plr, Item* i, SpellCastTargets const& targets)
    {
        if (plr->InBattleground())
        {
			plr->ModifyHonorPoints(5000);
            Unit* nuclear = plr->SummonCreature(BOMB, plr->GetPositionX(), plr->GetPositionY(), plr->GetPositionZ(),  plr->GetOrientation(), TEMPSUMMON_DEAD_DESPAWN,
            5000);
            nuclear->setFaction(plr->getFaction());
        }
        return false;
    }
};

/*
#define WARNING3 "До активации ядерного удара 3 секунды! ГОТОВЬТЕСЬ УМЕРЕТЬ"
#define WARNING5 "До активации точечного ядерного удара 5 секунд!"
#define BANG "ЯДЕРНАЯ ЗИМА!!!"
class npc_nuclear_bomb : public CreatureScript
{
public:
    npc_nuclear_bomb() : CreatureScript("npc_nuclear_bomb"){}
    
    struct nuclear_bombAI : public ScriptedAI
    {
        nuclear_bombAI(Creature* c) : ScriptedAI (c) { }

        uint32 warn5;
        uint32 warn1;
        uint32 desp;
        uint32 Bang;
        uint32 finishBG;
        void Reset()
        {
            warn5 = 5000;
            warn1 = 7000;
            Bang = 10000;
            desp = 15000;
            finishBG = 20000;
        }

        void UpdateAI(const uint32 diff)
        {
            if (warn1 <= diff)
            {
				warn1 = 7000;
                Map::PlayerList const& pList = me->GetMap()->GetPlayers();
                me->MonsterYell(WARNING3, LANG_UNIVERSAL, NULL);

				if (pList.isEmpty())
					return;

                for (Map::PlayerList::const_iterator itr = pList.begin(); itr != pList.end(); ++itr)
                    if (itr->getSource()->getFaction() != me->getFaction())
						itr->getSource()->HandleEmoteCommand(EMOTE_STATE_KNEEL);
            } else warn1 -= diff;

            if (warn5 <= diff)
            {
				warn5 = 5000;
                me->MonsterYell(WARNING5, LANG_UNIVERSAL, NULL);

            } else warn5 -= diff;

            if (Bang <= diff)
            {
				Bang = 10000;
                Map::PlayerList const& pList = me->GetMap()->GetPlayers();
                me->MonsterYell(BANG, LANG_UNIVERSAL, NULL);
				
				if (pList.isEmpty())
					return;

                for (Map::PlayerList::const_iterator itr = pList.begin(); itr != pList.end(); ++itr)
                    if (itr->getSource()->getFaction() != me->getFaction())
                        itr->getSource()->KillPlayer();
            }

            if (finishBG <= diff)
            {
				finishBG = 20000;
				if (me->GetOwner())
					me->GetOwner()->ToPlayer()->GetBattleground()->EndBattleground(me->GetOwner()->ToPlayer()->GetTeam());
            } else finishBG -= diff;

            if(desp <= diff)
            {
                desp = 15000;
                me->DespawnOrUnsummon();
            } else desp -= diff;
        }       
    };

    CreatureAI *GetAI(Creature *c) const
    {
        return new nuclear_bombAI(c);
    }
};        */

#define BUY_STR "Я хочу повысить свою силу на 10 едениц"
#define BUY_STAM "Я хочу повысить свою выносливость на 15 едениц"
#define BUY_AGI "Я хочу повысить свою ловкость на 10 едениц"
#define BUY_INTEL "Я хочу повысить свой интеллект на 10 едениц"
#define BUY_SPIRIT "Я хочу повысить свой дух на 10 едениц"
#define BUY_SPD "Я хочу повысить силу своей магии на 11 едениц"
#define TOTAL_UPS "Я хочу посмотреть сделанные усиления"
#define RESET "Сбросить мои усиления (стоимость: 2500 золота)"
#define SUCCSESS_BUY  "Поздравляю, $N , ты отдал дань и получил вознаграждение, во имя Плети!"
#define FAIL_BUY_H "Не достаточно очков усилений"

class npc_bonus_stats : public CreatureScript
{
public:
    npc_bonus_stats() : CreatureScript("npc_bonus_stats") { }
  

    bool OnGossipHello(Player* player, Creature* creature)
    {
		player->GainBonusUp();
		std::ostringstream st;
		st << "Количество доступных усилений: " << GetFreeUps(player);
		
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, st.str().c_str(), GOSSIP_SENDER_INFO, GOSSIP_ACTION_INFO_DEF + 1);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, BUY_STR , GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, BUY_AGI , GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 3);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, BUY_STAM , GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 4);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, BUY_INTEL , GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 5);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, BUY_SPIRIT , GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 6);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, BUY_SPD , GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 7);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, TOTAL_UPS , GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 8);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, RESET , GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 9);

		player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
		return true;
    }

    bool OnGossipSelect(Player* player, Creature* creature, uint32 /*uiSender*/, uint32 uiAction)
    {
        player->PlayerTalkClass->ClearMenus();

        switch (uiAction)
        {
            case GOSSIP_ACTION_INFO_DEF + 2:
			{
				if (incrementBonus(player, 0))
					creature->MonsterWhisper(SUCCSESS_BUY, player->GetGUID());
				else creature->MonsterWhisper(FAIL_BUY_H, player->GetGUID());
		        player->CLOSE_GOSSIP_MENU();
			   break;
			}
			case GOSSIP_ACTION_INFO_DEF + 3:
			{	
				if (incrementBonus(player, 1))
					creature->MonsterWhisper(SUCCSESS_BUY, player->GetGUID());
				else creature->MonsterWhisper(FAIL_BUY_H, player->GetGUID());
				player->CLOSE_GOSSIP_MENU();
				break;
			}
			case GOSSIP_ACTION_INFO_DEF + 4:
			{
				if (incrementBonus(player, 2))
					creature->MonsterWhisper(SUCCSESS_BUY, player->GetGUID());
				else creature->MonsterWhisper(FAIL_BUY_H, player->GetGUID());
				player->CLOSE_GOSSIP_MENU();
				break;
			}
			case GOSSIP_ACTION_INFO_DEF + 5:
			{
				if (incrementBonus(player, 3))
					creature->MonsterWhisper(SUCCSESS_BUY, player->GetGUID());
				else creature->MonsterWhisper(FAIL_BUY_H, player->GetGUID());
		        player->CLOSE_GOSSIP_MENU();	
				break;
			}
			case GOSSIP_ACTION_INFO_DEF + 6:
			{
				if (incrementBonus(player, 4))
					creature->MonsterWhisper(SUCCSESS_BUY, player->GetGUID());
				else creature->MonsterWhisper(FAIL_BUY_H, player->GetGUID());
		        player->CLOSE_GOSSIP_MENU();	
				break;
			}
			case GOSSIP_ACTION_INFO_DEF + 7:
			{
				if (incrementBonus(player, 5))
					creature->MonsterWhisper(SUCCSESS_BUY, player->GetGUID());
				else creature->MonsterWhisper(FAIL_BUY_H, player->GetGUID());
		        player->CLOSE_GOSSIP_MENU();
				break;
			}
			case GOSSIP_ACTION_INFO_DEF + 8:
			{
				creature->MonsterWhisper(ShowSpendUps(player).c_str(), player->GetGUID());
		        player->CLOSE_GOSSIP_MENU();
				break;
			}
			case GOSSIP_ACTION_INFO_DEF + 9:
			{
				if (player->HasEnoughMoney(25000000))
				{
					player->PlayerTalkClass->ClearMenus();
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Вы действительно хотите сбросить ваши усиления?", GOSSIP_SENDER_INFO, GOSSIP_ACTION_INFO_DEF + 10);
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "ДА, Я хочу сбросить свои усиления", GOSSIP_SENDER_INFO, GOSSIP_ACTION_INFO_DEF + 11);
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Нет", GOSSIP_SENDER_INFO, GOSSIP_ACTION_INFO_DEF + 12);
					player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
				}
				else player->SendBuyError(BUY_ERR_NOT_ENOUGHT_MONEY, 0, 0, 0);
				break;
			}
			case GOSSIP_ACTION_INFO_DEF + 11:
			{
				if (AllowReset(player))
				{
					player->ModifyMoney(-25000000);
					ResetUp(player);
				}
				creature->MonsterWhisper("У вас нет задействованных усилений", player->GetGUID());
		        player->CLOSE_GOSSIP_MENU();
				break;
			}
			case GOSSIP_ACTION_INFO_DEF + 12:
			{
				player->CLOSE_GOSSIP_MENU();
				break;
			}
			default: break;
        }
 
        return true;
    }

    bool incrementBonus(Player *pPlayer, int8 i)
    {
		if (GetFreeUps(pPlayer) > 0)
		{
			switch (i)
			{
				case 0:
					pPlayer->SetBonusStat(STAT_STRENGTH, pPlayer->GetBonusStat(STAT_STRENGTH) + 10);
					break;
				case 1:
					pPlayer->SetBonusStat(STAT_AGILITY, pPlayer->GetBonusStat(STAT_AGILITY) + 10);
					break;
				case 2:
					pPlayer->SetBonusStat(STAT_STAMINA, pPlayer->GetBonusStat(STAT_STAMINA) + 15);
					break;
				case 3:
					pPlayer->SetBonusStat(STAT_INTELLECT, pPlayer->GetBonusStat(STAT_INTELLECT) + 10);
					break;
				case 4:
					pPlayer->SetBonusStat(STAT_SPIRIT, pPlayer->GetBonusStat(STAT_SPIRIT) + 10);
					break;
				case 5:
					pPlayer->SetBonusSpd(pPlayer->GetBonusSpd() + 11);
					break;
				default: break;
			}
			pPlayer->UpdateAllStats();
			StoreUp(pPlayer);
			pPlayer->CastSpell(pPlayer, 73078, false);
			pPlayer->SaveToDB();
			return true;
		}
		else return false;
	}

	bool AllowReset(Player* player)
	{
		QueryResult res = CharacterDatabase.PQuery("Select spent_ups from character_bonus_stats where guid = %u", player->GetGUID());

		if (!res)
			return false;
		
		Field* fields = res->Fetch();
		uint32 ups = fields[0].GetUInt32();

		if (ups > 0)
			return true;
		return false;
	}

	uint32 GetFreeUps(Player* player)
	{
		if (!player)
			return 0;

		QueryResult res = CharacterDatabase.PQuery("Select up_count,spent_ups from character_bonus_stats where guid = %u", player->GetGUID());

		if (!res)
			return 0;

		Field* fields = res->Fetch();
		uint32 ups = fields[0].GetUInt32();
		uint32 spent = fields[1].GetUInt32();
		ups -= spent;

		return ups;
	}

	void StoreUp(Player* player)
	{
		QueryResult res = CharacterDatabase.PQuery("Select spent_ups from character_bonus_stats where guid = %u", player->GetGUID());

		if (!res)
			return;

		uint32 spent = 0;

		Field* fields = res->Fetch();
		spent = fields[0].GetUInt32();

		CharacterDatabase.PExecute("Update character_bonus_stats SET spent_ups = '%u' where guid = '%u'", spent+1, player->GetGUID());
	}

	std::string ShowSpendUps(Player* player)
	{
		QueryResult res = CharacterDatabase.PQuery("SELECT str, agl, stam, intel, spt, spd from character_bonus_stats where guid = %u", player->GetGUID());

		if (!res)
			return "У вас нет усилений.";

		Field* fields = res->Fetch();
		uint32 str = fields[0].GetUInt32();
		uint32 agl = fields[1].GetUInt32();
		uint32 stam = fields[2].GetUInt32();
		uint32 intel = fields[3].GetUInt32();
		uint32 spt = fields[4].GetUInt32();
		uint32 spd = fields[5].GetUInt32();

		std::ostringstream ss;
		ss << "Сейчас у вас "; 
			if (str > 0) ss << str << " бонусных едениц силы ";
			if (agl > 0) ss << agl << " бонусных едениц ловкости ";
			if (stam > 0) ss << stam << " бонусных едениц выносливости ";
			if (intel > 0) ss << intel << " бонусных едениц интеллекта ";
			if (spt > 0) ss << spt << " бонусных едениц духа ";
			if (spd > 0) ss << spd << " бонусных едениц силы заклинаний.";

		if (str == 0 && agl == 0 && stam == 0 && intel == 0 && spt == 0 && spd == 0)
			ss << "нет бонусных характеристик";

		return ss.str();
	}

	void ResetUp(Player* player)
	{
		CharacterDatabase.PExecute("Update character_bonus_stats SET spent_ups = '0', spd = '0', str = '0', intel = '0', spt = '0', agl = '0', stam = '0'  where guid = '%u'", player->GetGUID());
		player->SetBonusSpd(0);
		player->SetBonusStat(STAT_STRENGTH, 0);
		player->SetBonusStat(STAT_AGILITY, 0);
		player->SetBonusStat(STAT_STAMINA, 0);
		player->SetBonusStat(STAT_INTELLECT, 0);
		player->SetBonusStat(STAT_SPIRIT, 0);
		player->UpdateAllStats();
		player->SaveToDB();
	}
};

class achiev_first_on_server : public PlayerScript
{
public:
  achiev_first_on_server() : PlayerScript("achiev_first_on_server") {}
  

  void OnLevelChanged(Player* player, uint8 newLevel)
  {
      if(player && player->getLevel() == 80)
      {
		  switch(player->getRace())
		  {
		  case RACE_NAGA:
          {
			  QueryResult res = CharacterDatabase.PQuery("SELECT guid FROM character_achievement WHERE achievement = 6000;");
			  if(!res)
			    if (AchievementEntry const* achievementEntry = sAchievementStore.LookupEntry(6000))
					player->CompletedAchievement(achievementEntry);

			  break;
          }
		  case RACE_WORGEN:
          {
			  QueryResult res = CharacterDatabase.PQuery("SELECT guid FROM character_achievement WHERE achievement = 6001;");
			  if(!res)
				if (AchievementEntry const* achievementEntry = sAchievementStore.LookupEntry(6001))
                    player->CompletedAchievement(achievementEntry);

			  break;
          }
		  case RACE_QUELDO:
          {
			  QueryResult res = CharacterDatabase.PQuery("SELECT guid FROM character_achievement WHERE achievement = 6002;");
			  if(!res)
				if (AchievementEntry const* achievementEntry = sAchievementStore.LookupEntry(6002))
                    player->CompletedAchievement(achievementEntry);

			  break;
          }
		  case RACE_GOBLIN:
          {
			  QueryResult res = CharacterDatabase.PQuery("SELECT guid FROM character_achievement WHERE achievement = 6003;");
			  if(!res)
				if (AchievementEntry const* achievementEntry = sAchievementStore.LookupEntry(6003))
                    player->CompletedAchievement(achievementEntry);

			  break;
          }
		  case RACE_BROKEN:
          {
			  QueryResult res = CharacterDatabase.PQuery("SELECT guid FROM character_achievement WHERE achievement = 6004;");
			  if(!res)
				if (AchievementEntry const* achievementEntry = sAchievementStore.LookupEntry(6004))
                    player->CompletedAchievement(achievementEntry);

			  break;
          }
		  case RACE_VRYKUL:
          {
			  QueryResult res = CharacterDatabase.PQuery("SELECT guid FROM character_achievement WHERE achievement = 6005;");
			  if(!res)
				if (AchievementEntry const* achievementEntry = sAchievementStore.LookupEntry(6005))
					player->CompletedAchievement(achievementEntry);

			  break;
          }
		  default:
			  break;
		  }
      }
  }

};

enum ePredator
{
    NPC_PREDATOR = 50009,


};


class npc_bg_predator : public CreatureScript
{
    public:
    npc_bg_predator() : CreatureScript("npc_bg_predator") { }

    CreatureAI *GetAI(Creature *creature) const
    {
        return new npc_bg_predatorAI(creature);
    }

    struct npc_bg_predatorAI : public ScriptedAI
    {
        npc_bg_predatorAI(Creature* pCreature) : ScriptedAI(pCreature), Summons(me) { }
        uint32 uiSpawn;
        uint32 uiTele;
        Creature*   cPredator;
        Player* pPlayer;

        SummonList Summons;

        
        void Reset()
        {           
            uiSpawn = 1000;
            uiTele = 1500;
            Summons.DespawnAll();
        }

        void JustSummoned(Creature* summoned)
        {
            Summons.Summon(summoned);
        }     

        void UpdateAI(const uint32 diff)
        {
            if(uiSpawn <= diff )
            {   
                cPredator = me->SummonCreature(NPC_PREDATOR, me->GetPositionX(), me->GetPositionY(), me->GetPositionZ()+55, 0.0f, TEMPSUMMON_TIMED_DESPAWN, 240000);
                if(TempSummon* tsSummon = me->ToTempSummon())
                    if(Unit* uSummoner = tsSummon->GetSummoner())
                         if (uSummoner->GetTypeId() == TYPEID_PLAYER) 
                             if (pPlayer = uSummoner->ToPlayer()) 
                                 pPlayer->EnterVehicle(cPredator);


                uiSpawn = 999999999; 
            } else uiSpawn -= diff;

            if(uiTele <= diff )
            {
                if(Summons.empty())
                    if(pPlayer)
                    {
                        pPlayer->TeleportTo(me->GetMapId(), me->GetPositionX(), me->GetPositionY(), me->GetPositionZ(), 0);
                        me->DespawnOrUnsummon();
                    }

            } else uiTele -= diff;

        }
    };
};



class npc_bg_predator_vehicle : public CreatureScript
{
    public:
    npc_bg_predator_vehicle() : CreatureScript("npc_bg_predator_vehicle") { }

    CreatureAI *GetAI(Creature *creature) const
    {
        return new npc_bg_predator_vehicleAI(creature);
    }

    struct npc_bg_predator_vehicleAI : public ScriptedAI
    {
        npc_bg_predator_vehicleAI(Creature* pCreature) : ScriptedAI(pCreature) { }

        uint32 uiDestroy;


        void Reset()
        {   
            uiDestroy = 999999999; 
        }

        void SpellHitTarget(Unit* target, const SpellInfo* siSpell)
        {
            uiDestroy = 5000;
        }

        void UpdateAI(const uint32 diff)
        {
            if(uiDestroy <= diff )
            {
                me->DespawnOrUnsummon();
                uiDestroy = 999999999; 
            } else uiDestroy -= diff;
        }
    };
};




void AddSC_scourge()
{
    new item_bg_orbital_strike();
    new item_bg_btr();
    new item_bg_flying_machine();
    new npc_bg_mine();
    new npc_bg_c4();
    new npc_bg_turret();
    new npc_bg_fort_guard();
    new npc_bg_air_strike();
    new npc_nuclear_bomb;
    new nuclear_bomb;
    new npc_emblem_changer();
    new npc_sportcar_trader();
    new PsEventStuff();
    new EventTriggerTeleporter();
    new teleport_on_click();
    new event_start1();
    new npc_healtarget1();
    new npc_damager1();
    new npc_damager2();
    new npc_bonus_stats();
    new achiev_first_on_server();
    new npc_bg_predator_vehicle();
    new npc_bg_predator();
}
